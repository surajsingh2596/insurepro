package com.futurity.insurepro.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CommonResponseModel<T> implements Serializable {
    @SerializedName("status")
    @Expose
    private int status;

    @SerializedName("data")
    @Expose
    private T data;

    public CommonResponseModel() {
    }

    public int  getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
