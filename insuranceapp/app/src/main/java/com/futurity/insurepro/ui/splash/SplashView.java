package com.futurity.insurepro.ui.splash;

import com.futurity.insurepro.model.LoginResponseModel;
import com.futurity.insurepro.ui.base.BaseView;

public interface SplashView extends BaseView {
   void loginActivity();
   void sendOtpActivity();
   void dashboardActivity();
   void showProgressD();
   void hideProgressD();
   void error(String error);
   void loginSuccess(LoginResponseModel loginResponseModel);
}
