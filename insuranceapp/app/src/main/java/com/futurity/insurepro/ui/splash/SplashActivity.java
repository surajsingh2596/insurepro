package com.futurity.insurepro.ui.splash;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.futurity.insurepro.R;
import com.futurity.insurepro.components.GlobalConstant;
import com.futurity.insurepro.model.LoginResponseModel;
import com.futurity.insurepro.model.UserModel;
import com.futurity.insurepro.model.UserProfileModel;
import com.futurity.insurepro.ui.base.BaseActivity;
import com.futurity.insurepro.ui.login.LoginActivity;
import com.futurity.insurepro.ui.main.MainActivity;
import com.futurity.insurepro.ui.sendOtp.SendOtpActivity;


public class SplashActivity extends BaseActivity implements SplashView {

    SplashPresenter<SplashView> mPresenter;
    private static String TAG="SplashActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        mPresenter=new SplashPresenter<>(this);
        mPresenter.onAttach(this);
    }

    @Override
    public void dashboardActivity() {
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    public void loginActivity() {
        Intent i = new Intent(this, LoginActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    public void sendOtpActivity() {
        Intent i = new Intent(this, SendOtpActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    public void showProgressD() {
        ProgressBar progressBar=findViewById(R.id.progress_bar_d);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressD() {
        ProgressBar progressBar=findViewById(R.id.progress_bar_d);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void loginSuccess(LoginResponseModel loginResponseModel) {
        UserProfileModel userProfileModel=new UserProfileModel();
        userProfileModel.setFirstname(loginResponseModel.getName());
        userProfileModel.setId(loginResponseModel.getUserId());
        userProfileModel.setRedirectUrl(loginResponseModel.getUrl());
        userProfileModel.setProfileImage(loginResponseModel.getProfilePic());
        userProfileModel.setUsername(loginResponseModel.getUsername());
        userProfileModel.setPassword(GlobalConstant.DEFAULT_PASSWORD);

        UserModel.setRid(this,loginResponseModel.getRid());
        UserModel.login(this,userProfileModel);
        Log.d(TAG, "loginSuccess: "+UserModel.getUser(this).getProfileImage());
        Intent i = new Intent(SplashActivity.this, MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(i);
    }

    @Override
    public void error(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
        forceLogout(getString(R.string.continue_login));
    }

    @Override
    public void setTitle() {

    }


    @Override
    public void showLoading() {
        LinearLayout mainContent=findViewById(R.id.progress_bar);
        mainContent.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        LinearLayout mainContent=findViewById(R.id.progress_bar);
        mainContent.setVisibility(View.GONE);
    }

    @Override
    public void isNetworkConnected(boolean val) {
        Toast.makeText(this, getString(R.string.internet_connection_issue), Toast.LENGTH_LONG).show();
        onShowCommonErrorPage();
    }

    @Override
    public void onShowMainContent() {
        LinearLayout mainContent=findViewById(R.id.main_content);
        mainContent.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHideMainContent() {
        LinearLayout mainContent=findViewById(R.id.main_content);
        mainContent.setVisibility(View.GONE);
    }

    @Override
    public void onShowCommonErrorPage() {
        LinearLayout mainContent=findViewById(R.id.page_error);
        mainContent.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHideCommonErrorPage() {
        LinearLayout mainContent=findViewById(R.id.page_error);
        mainContent.setVisibility(View.GONE);
    }

    @Override
    public void onShowNoData() {

    }

    @Override
    public void onHideNoData() {

    }

    @Override
    public void logoutUser() {

    }
    public void logout(){
        super.logout();
    }

    @Override
    public void hideKeyboard() {
        GlobalConstant.hideSoftKeyboard(this);
    }

    @Override
    public void forceLogout(String message) {
        super.forceLogout(message);
    }
    @Override
    public void hideAll() {
      onHideCommonErrorPage();
      onHideMainContent();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}
