package com.futurity.insurepro.ui.sendOtp;


import com.futurity.insurepro.model.SendOtpModel;
import com.futurity.insurepro.ui.base.BaseView;

public interface SendOtpView extends BaseView {
    void getSuccess(SendOtpModel sendOtpModel);
    void getFailure(String error);
}
