package com.futurity.insurepro.ui.main;



import android.location.Location;

import com.futurity.insurepro.model.NavigationDataModel;
import com.futurity.insurepro.model.UserProfileModel;
import com.futurity.insurepro.ui.base.BaseView;

import java.util.List;


public interface MainView extends BaseView {

    void getUserModel(UserProfileModel userProfileModel);
    void closeNavigationDrawer();
    void openNavigationDrawer();
    void error(String message);
    void navigationDataSuccess(List<NavigationDataModel> navigationDataModels);
    void getNavigationItem(NavigationDataModel navigationDataModel);
    void showWebviewError();
    void hideWebviewError();

    void showNavigationItem();
    void showNavigationLoader();
    void showNavigationReload();
    void hideNavigationItem();
    void hideNavigationLoader();
    void hideNavigationReload();
    void hideNavigationAll();

    void setUserFullName(String name);
    void ifLocationPermissionGranted(boolean b);
    void ifStoragePermissionGranted(boolean b);
    void getCurrentLocation(Location location);
    void updateUserProfileImage(String path);
}
