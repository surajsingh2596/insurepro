package com.futurity.insurepro.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class OtpMessageResponse{
    @SerializedName("ErrorCode")
    @Expose
    public String errorCode;

    @SerializedName("ErrorMessage")
    @Expose
    public String errorMessage;

    @SerializedName("JobId")
    @Expose
    public String jobId;

    @SerializedName("MessageData")
    @Expose
    public List<MessageData> messageData;

    public OtpMessageResponse() {
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public List<MessageData> getMessageData() {
        return messageData;
    }

    public void setMessageData(List<MessageData> messageData) {
        this.messageData = messageData;
    }

    public static class MessageData{
        @SerializedName("Number")
        @Expose
        public String number;

        @SerializedName("MessageId")
        @Expose
        public String messageId;

        @SerializedName("Message")
        @Expose
        public String message;

        public MessageData() {
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getMessageId() {
            return messageId;
        }

        public void setMessageId(String messageId) {
            this.messageId = messageId;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

}
