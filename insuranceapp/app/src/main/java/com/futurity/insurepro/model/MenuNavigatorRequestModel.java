package com.futurity.insurepro.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MenuNavigatorRequestModel extends CommonLoginRequestModel implements Serializable {

    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("rid")
    @Expose
    private int rid;

    public MenuNavigatorRequestModel() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getRid() {
        return rid;
    }

    public void setRid(int rid) {
        this.rid = rid;
    }
}
