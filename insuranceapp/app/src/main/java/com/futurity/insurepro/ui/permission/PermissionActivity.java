package com.futurity.insurepro.ui.permission;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;

import com.futurity.insurepro.R;
import com.futurity.insurepro.broadcastReceiver.AppSMSBroadcastReceiver;
import com.futurity.insurepro.components.GlobalConstant;
import com.futurity.insurepro.model.PermissionModel;
import com.futurity.insurepro.model.SendOtpModel;
import com.futurity.insurepro.model.UserModel;
import com.futurity.insurepro.ui.base.BaseActivity;
import com.futurity.insurepro.ui.login.LoginActivity;
import com.futurity.insurepro.ui.sendOtp.SendOtpActivity;
import com.mukesh.OtpView;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class PermissionActivity extends BaseActivity implements PermissionView {

    PermissionPresenter<PermissionView> mPresenter;
    private static String TAG="SendOtpactivity";
    SendOtpModel sendOtpModel;
    private OtpView pin;
    private IntentFilter intentFilter;

    private AppSMSBroadcastReceiver appSMSBroadcastReceiver;
    List<PermissionModel> mPermissionModelList;
    private static final int APP_DRAW_CODE=23465;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ifNotCheckPermission=true;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.permission_activity);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            Window w = getWindow();
//            w.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
//            w.setStatusBarColor(getColor(R.color.enter_mobile));
//        }
        mPresenter=new PermissionPresenter<>(this);
        mPresenter.onAttach(this);

    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.checkPermissions();
        mPresenter.checkOverlay();
        mPresenter.checkAllPermit();
    }

    @Override
    public void getAllPermit(boolean b) {
        if(b){
            Intent resultIntent = new Intent();
            setResult(Activity.RESULT_OK, resultIntent);
            finish();
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if( mPresenter.getAllPermission()){
            Intent resultIntent = new Intent();
            setResult(Activity.RESULT_OK, resultIntent);
        }else{
            if(UserModel.isLogin(this)){
                UserModel.logout(PermissionActivity.this);
                finishAffinity();
            }else{
                finishAffinity();
            }
        }
    }
    public void logout(){
        GlobalConstant.showMessageOKCancel(this, getString(R.string.want_logout), new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                Class<?> aClass=null;
                if(GlobalConstant.LOGIN_TYPE==1){
                    aClass=LoginActivity.class;
                }else if(GlobalConstant.LOGIN_TYPE==2){
                    aClass= SendOtpActivity.class;
                }

                if(aClass!=null){
                    UserModel.logout(PermissionActivity.this);
                    finishAffinity();
                }

            }
        });
    }
    public void onExplore(View view){
        switch(view.getId()){
            case R.id.bt_enabled:
                break;
            case R.id.bt_disabled:
                onClickDisable();
                break;
            case R.id.bt_disabled_setting:
                onClickDisableSetting();
                break;
            case R.id.bt_overlay_disabled:
                onClickOverlay();
                break;
        }
    }

    @Override
    public void onClickEnable() {

    }

    @Override
    public void onClickDisable() {
      if(mPermissionModelList!=null){
          List<String> listPermissionsNeeded = new ArrayList<>();
          for(PermissionModel permissionModel:mPermissionModelList){
              if(permissionModel.getType()==PermissionModel.TYPE_DENIED){
                  listPermissionsNeeded.add(permissionModel.getPermission());
              }

          }
          if(listPermissionsNeeded.size()>0){
              ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),MarshMallowPermission.READ_PHONE_STATE_PERMISSION_REQUEST_CODE_CALL_LOG );
          }
      }
    }

    @Override
    public void onClickOverlay() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.canDrawOverlays(this)) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:com.futurity.com.futurity.insurepro"));
                startActivityForResult(intent, APP_DRAW_CODE);
            }
        }
    }

    @Override
    public void onClickDisableSetting() {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivity(intent);
    }

    @Override
    public void getPermissionList(List<PermissionModel> permissionModelList) {
        mPermissionModelList=permissionModelList;
    }

    @Override
    public void showButtonEnabled() {
        LinearLayout linearLayout=findViewById(R.id.bt_enabled);
        linearLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void showButtonDisabled() {
        LinearLayout linearLayout=findViewById(R.id.bt_disabled);
        linearLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void showButtonDisabledSetting() {
        LinearLayout linearLayout=findViewById(R.id.bt_disabled_setting);
        linearLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideButtonEnabled() {
        LinearLayout linearLayout=findViewById(R.id.bt_enabled);
        linearLayout.setVisibility(View.GONE);
    }

    @Override
    public void hideButtonDisabled() {
        LinearLayout linearLayout=findViewById(R.id.bt_disabled);
        linearLayout.setVisibility(View.GONE);
    }

    @Override
    public void hideButtonDisabledSetting() {
        LinearLayout linearLayout=findViewById(R.id.bt_disabled_setting);
        linearLayout.setVisibility(View.GONE);
    }

    @Override
    public void hideAllButtons() {
        hideButtonDisabled();
        hideButtonEnabled();
        hideButtonDisabledSetting();
    }

    @Override
    public void showOverLayEnabled() {
        LinearLayout linearLayout=findViewById(R.id.bt_overlay_enabled);
        linearLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideOverlayEnabled() {
        LinearLayout linearLayout=findViewById(R.id.bt_overlay_enabled);
        linearLayout.setVisibility(View.GONE);
    }

    @Override
    public void showOverlayDisabled() {
        LinearLayout linearLayout=findViewById(R.id.bt_overlay_disabled);
        linearLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideOverlayDisabled() {
        LinearLayout linearLayout=findViewById(R.id.bt_overlay_disabled);
        linearLayout.setVisibility(View.GONE);
    }

    @Override
    public void hideAllOverlayButtons() {
        hideOverlayEnabled();
        hideOverlayDisabled();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MarshMallowPermission.READ_PHONE_STATE_PERMISSION_REQUEST_CODE_CALL_LOG:
                for(String permission:permissions){
                    if(ActivityCompat.shouldShowRequestPermissionRationale(this, permission)){
                    //denied
                        setPermissionType(permission,PermissionModel.TYPE_DENIED);
                    Log.d(TAG, "onRequestPermissionsResult: rational "+permission);
                    }else{
                    if(ActivityCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED){
                        //allowed
                        setPermissionType(permission,PermissionModel.TYPE_ALLOWED);
                        Log.d(TAG, "onRequestPermissionsResult: granted "+permission);
                    } else{
                        //set to never ask again
                        setPermissionType(permission,PermissionModel.TYPE_SET_NEVER_ASK_AGAIN);
                        Log.d(TAG, "onRequestPermissionsResult: granted not "+permission);
                        //openSettings();
                    }
                }
                }
                break;
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (requestCode == APP_DRAW_CODE) {
                if (Settings.canDrawOverlays(this)) {

                    Log.d(TAG, "onActivityResult: ssss");
                }else{
                    mPresenter.drawOverlay(false);
                }
            }
        }
        mPresenter.drawOverlay(true);
    }
    @Override
    public void setPermissionType(String permission, int type) {
        Iterator<PermissionModel> iterator=mPermissionModelList.iterator();
        while (iterator.hasNext()){
            PermissionModel permissionModel=iterator.next();
            if(permissionModel.getPermission().equals(permission)){
                permissionModel.setType(type);
                break;
            }

        }
        mPresenter.setPermissionModelList(mPermissionModelList);
    }

    @Override
    public void setTitle() {

    }

    @Override
    public void showLoading() {
        LinearLayout mainContent=findViewById(R.id.progress_bar);
        mainContent.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        LinearLayout mainContent=findViewById(R.id.progress_bar);
        mainContent.setVisibility(View.GONE);
    }

    @Override
    public void isNetworkConnected(boolean val) {
        Toast.makeText(this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onShowMainContent() {
        LinearLayout mainContent=findViewById(R.id.main_content);
        mainContent.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHideMainContent() {
        LinearLayout mainContent=findViewById(R.id.main_content);
        mainContent.setVisibility(View.GONE);
    }

    @Override
    public void onShowCommonErrorPage() {

    }

    @Override
    public void onHideCommonErrorPage() {

    }

    @Override
    public void logoutUser() {

    }

    @Override
    public void hideKeyboard() {
        GlobalConstant.hideSoftKeyboard(this);
    }

    @Override
    public void forceLogout(String message) {

    }

    @Override
    public void onShowNoData() {

    }

    @Override
    public void onHideNoData() {

    }

    @Override
    public void hideAll() {

    }
}
