package com.futurity.insurepro.ui.base;


public interface UserActivityView extends BaseView {
    void adminActivity();
    void entryModule();
    void exitModule();
    void forceLogout();
}
