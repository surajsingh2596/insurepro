package com.futurity.insurepro.ui.main;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.DownloadListener;
import android.webkit.GeolocationPermissions;
import android.webkit.URLUtil;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.futurity.insurepro.R;
import com.futurity.insurepro.components.GlobalConstant;
import com.futurity.insurepro.components.JavaScriptInterface;
import com.futurity.insurepro.gps.GpsUtils;
import com.futurity.insurepro.model.CurrentLocationModel;
import com.futurity.insurepro.model.NavigationDataModel;
import com.futurity.insurepro.model.NotificationResponseModel;
import com.futurity.insurepro.model.UserProfileModel;
import com.futurity.insurepro.recyclerView.NavigationDataAdapter;
import com.futurity.insurepro.services.CurrentLocationUpdate;
import com.futurity.insurepro.ui.base.BaseActivity;
import com.futurity.insurepro.ui.permission.LocationPermissionActivity;
import com.futurity.insurepro.ui.permission.MarshMallowPermission;
import com.futurity.insurepro.ui.permission.StoragePermissionActivity;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class MainActivity extends BaseActivity implements MainView {

    MainPresenter<MainView> mPresenter;
    private static String TAG="MainActivity";
    final int PERMISSION_RESULT = 37;
    final int PERMISSION_RESULT_STORAGE = 39;
    final int PERMISSION_RESULT_ACTIVITY = 39;
    private static final int INTENT_PROFILE_UPDATE = 24;
    DrawerLayout drawer;
    WebView webView;

    private NavigationDataAdapter navigationDataAdapter;
    private List<NavigationDataModel> navigationDataModels=new ArrayList<>();
    private RecyclerView recyclerViewSite;
    private UserProfileModel userProfileModel;
    private NavigationDataModel currentNavigationDataModel;

    private ValueCallback<Uri> mUploadMessage;
    private ValueCallback<Uri[]> mUMA;
    private String mCM;
   // private final static int FILECHOOSER_RESULTCODE = 1;

    public static final int REQUEST_SELECT_FILE = 100;
    private final static int FILECHOOSER_RESULTCODE = 1;


    //private ValueCallback<Uri> mUploadMessage;
    private Uri mCapturedImageURI = null;
    private ValueCallback<Uri[]> mFilePathCallback;
    private String mCameraPhotoPath;
    private static final int INPUT_FILE_REQUEST_CODE = 1;
    //private static final int FILECHOOSER_RESULTCODE = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ifDoubleBack=true;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_dashboard);

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            Window w = getWindow();
//            w.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
//            w.setStatusBarColor(getColor(R.color.enter_mobile));
//        }
        recyclerViewSite=findViewById(R.id.navigation_data);
        setDrawable();
        setRecyclerView();
        mPresenter=new MainPresenter<>(this);
        mPresenter.onAttach(this);
        mPresenter.checkForLocationPermission();
        getDataFromNotification(this.getIntent());


    }

    private void getDataFromNotification(Intent intent){
        Bundle extras = intent.getExtras();
        if(extras!=null){
            NotificationResponseModel notificationResponseModel = (NotificationResponseModel) extras.getSerializable(GlobalConstant.NOTIFICATION_RESPONSE_MODEL);
            if(notificationResponseModel.getUrl()!=null && notificationResponseModel.getUrl().length()>0){
                webView.loadUrl(notificationResponseModel.getUrl());
                showLoading();
            }
        }else{
            mPresenter.getNavigationData();
            loadWebView();
        }

    }
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        getDataFromNotification(intent);
    }

    @Override
    public void setUserFullName(String name) {
        TextView textView=findViewById(R.id.name);
        textView.setText(name);
    }

    @Override
    public void updateUserProfileImage(String path) {
        ImageView profilePicDash=findViewById(R.id.profile_image_dash);

        Glide.with(getApplicationContext())
                .load(path)
                .fitCenter()
                .apply(RequestOptions.circleCropTransform())
                .placeholder(R.mipmap.avatar4)
                .error(R.mipmap.avatar4)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object o, Target<Drawable> target, boolean b) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable drawable, Object o, Target<Drawable> target, DataSource dataSource, boolean b) {
                        return false;
                    }
                })
                .into(profilePicDash);

    }
    @Override
    public void showNavigationItem() {
        RecyclerView recyclerView=findViewById(R.id.navigation_data);
        recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showNavigationLoader() {
        TextView textView=findViewById(R.id.text_loader);
        textView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showNavigationReload() {
      LinearLayout linearLayout=findViewById(R.id.re_load_navigation_page);
      linearLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideNavigationItem() {
        RecyclerView recyclerView=findViewById(R.id.navigation_data);
        recyclerView.setVisibility(View.GONE);
    }

    @Override
    public void hideNavigationLoader() {
        TextView textView=findViewById(R.id.text_loader);
        textView.setVisibility(View.GONE);
    }

    @Override
    public void hideNavigationReload() {
        LinearLayout linearLayout=findViewById(R.id.re_load_navigation_page);
        linearLayout.setVisibility(View.GONE);
    }

    public void hideNavigationAll(){
        hideNavigationItem();
        hideNavigationLoader();
        hideNavigationReload();
    }
    private void setDrawable(){
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
    }
    public void onExplore(View view){
       switch (view.getId()){
           case R.id.re_load_page:
               loadWebView();
               break;
           case R.id.drawer_button:
               openNavigationDrawer();
               break;
           case R.id.re_load_navigation_page:
               mPresenter.getNavigationData();
               break;


       }
    }
    private void setRecyclerView(){
        navigationDataAdapter = new NavigationDataAdapter(navigationDataModels, this,this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL,false);
        recyclerViewSite.setLayoutManager(mLayoutManager);
        recyclerViewSite.setItemAnimator(new DefaultItemAnimator());
        recyclerViewSite.setHasFixedSize(false);
        recyclerViewSite.setAdapter(navigationDataAdapter);

    }
    @Override
    public void closeNavigationDrawer() {
        drawer.closeDrawer(GravityCompat.START);
    }
    @Override
    public void openNavigationDrawer() {
        drawer.openDrawer(GravityCompat.START);
    }


    @Override
    public void setTitle() {

    }

    @Override
    public void getUserModel(UserProfileModel mUserProfileModel) {
        userProfileModel=mUserProfileModel;
    }

    public class ChromeClient extends WebChromeClient {

        @Override
        public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
            super.onGeolocationPermissionsShowPrompt(origin, callback);
            callback.invoke(origin, true, false);
        }

        // For Android 5.0
        public boolean onShowFileChooser(WebView view, ValueCallback<Uri[]> filePath, WebChromeClient.FileChooserParams fileChooserParams) {

            Log.d(TAG, "onShowFileChooser: ");
            MarshMallowPermission marshMallowPermission=new MarshMallowPermission(MainActivity.this);
            if(!marshMallowPermission.checkPermissionForExternalStorage()){
                ifStoragePermissionGranted(false);
                return false;
            }else{

                // Double check that we don't have any existing callbacks
                if (mFilePathCallback != null) {
                    mFilePathCallback.onReceiveValue(null);
                }
                mFilePathCallback = filePath;

                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    // Create the File where the photo should go
                    File photoFile = null;
                    try {
                        photoFile = createImageFile();
                        takePictureIntent.putExtra("PhotoPath", mCameraPhotoPath);
                    } catch (IOException ex) {
                        // Error occurred while creating the File
                        Log.e("ErrorCreatingFile", "Unable to create Image File", ex);
                    }

                    // Continue only if the File was successfully created
                    if (photoFile != null) {
                        Uri photoURI = FileProvider.getUriForFile(MainActivity.this, "com.futurity.com.futurity.insurepro.provider", photoFile);
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                photoURI);

                        mCameraPhotoPath = "file:" + photoFile.getAbsolutePath();
//                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
//                                Uri.fromFile(photoFile));
                    } else {
                        takePictureIntent = null;
                    }
                }

                Intent contentSelectionIntent = new Intent(Intent.ACTION_GET_CONTENT);
                contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE);
                contentSelectionIntent.setType("image/*");

                Intent[] intentArray;
                if (takePictureIntent != null) {
                    intentArray = new Intent[]{takePictureIntent};
                } else {
                    intentArray = new Intent[0];
                }

                Intent chooserIntent = new Intent(Intent.ACTION_CHOOSER);
                chooserIntent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent);
                chooserIntent.putExtra(Intent.EXTRA_TITLE, "Image Chooser");
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);

                startActivityForResult(chooserIntent, INPUT_FILE_REQUEST_CODE);

                return true;
            }
        }

        // openFileChooser for Android 3.0+
        public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType) {
            Log.d(TAG, "openFileChooser: ");
            MarshMallowPermission marshMallowPermission=new MarshMallowPermission(MainActivity.this);
            if(!marshMallowPermission.checkPermissionForExternalStorage()){
                ifStoragePermissionGranted(false);
            }else{
                mUploadMessage = uploadMsg;
                // Create AndroidExampleFolder at sdcard
                // Create AndroidExampleFolder at sdcard

                File imageStorageDir = new File(
                        Environment.getExternalStoragePublicDirectory(
                                Environment.DIRECTORY_PICTURES)
                        , "AndroidExampleFolder");

                if (!imageStorageDir.exists()) {
                    // Create AndroidExampleFolder at sdcard
                    imageStorageDir.mkdirs();
                }

                // Create camera captured image file path and name
                File file = new File(
                        imageStorageDir + File.separator + "IMG_"
                                + String.valueOf(System.currentTimeMillis())
                                + ".jpg");

                mCapturedImageURI = Uri.fromFile(file);

                // Camera capture image intent
                final Intent captureIntent = new Intent(
                        android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

                captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);

                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.addCategory(Intent.CATEGORY_OPENABLE);
                i.setType("image/*");

                // Create file chooser intent
                Intent chooserIntent = Intent.createChooser(i, "Image Chooser");

                // Set camera intent to file chooser
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS
                        , new Parcelable[] { captureIntent });

                // On select image call onActivityResult method of activity
                startActivityForResult(chooserIntent, FILECHOOSER_RESULTCODE);
            }
        }

        // openFileChooser for Android < 3.0
        public void openFileChooser(ValueCallback<Uri> uploadMsg) {
            openFileChooser(uploadMsg, "");
        }

        //openFileChooser for other Android versions
        public void openFileChooser(ValueCallback<Uri> uploadMsg,
                                    String acceptType,
                                    String capture) {

            openFileChooser(uploadMsg, acceptType);
        }

    }

    private void loadWebView(){
        showLoading();
        webView = (WebView) findViewById(R.id.webView1);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setAllowContentAccess(true);
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setGeolocationEnabled(true);
        webView.addJavascriptInterface(new JavaScriptInterface(this), "Android");
        webView.loadUrl(userProfileModel.getRedirectUrl());
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.setWebViewClient(new WebViewClient(){

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.startsWith("tel:")|| url.startsWith("whatsapp:") || url.startsWith("intent://") || url.startsWith("http://")) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(url));
                    startActivity(intent);
                    view.reload();
                    return true;
                }

                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                // do your stuff here
                hideLoading();
                Log.d(TAG, "onPageFinished: ");
               // mPresenter.getNavigationData();
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
//                hideAll();
//                showWebviewError();
//                Log.d(TAG, "onReceivedError: ");
            }

        });

        webView.setWebChromeClient(new ChromeClient());

        // download file and images
        webView.setDownloadListener(new DownloadListener() {

            public void onDownloadStart(String url, String userAgent,
                                        String contentDisposition, String mimetype,
                                        long contentLength) {

                MarshMallowPermission marshMallowPermission=new MarshMallowPermission(MainActivity.this);
                if(!marshMallowPermission.checkPermissionForExternalStorage()){
                    ifStoragePermissionGranted(false);
                }else{
                    downloadFile(url,userAgent,contentDisposition,mimetype,contentLength);
                }

            }

        });

    }

    private void downloadFile(String url, String userAgent,
                              String contentDisposition, String mimetype,
                              long contentLength){
        try{
            switch (mimetype){
                case "application/pdf":
                    webView.loadUrl(JavaScriptInterface.getBase64StringFromBlobUrl(url));
                    break;
                case "text/csv":
                    webView.loadUrl(JavaScriptInterface.getBase64StringFromBlobUrlCsv(url));
                    break;
                case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                    webView.loadUrl(JavaScriptInterface.getBase64StringFromBlobUrlExcel(url));
                    break;
                case "application/octet-stream":
                    JavaScriptInterface.convertBase64StringToOctetStreamAndStoreIt(url,MainActivity.this);
                    break;

                default:
                    String[] otpArray=mimetype.split("/");
                    if(otpArray.length>0){
                        if(otpArray[0].equals("image")){
                            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
                            request.allowScanningByMediaScanner();

                            request.setNotificationVisibility(
                                    DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

                            request.setDestinationInExternalPublicDir(
                                    Environment.DIRECTORY_DOWNLOADS,    //Download folder
                                    URLUtil.guessFileName(url, contentDisposition, mimetype));                        //Name of file
                            DownloadManager dm = (DownloadManager) getSystemService(
                                    DOWNLOAD_SERVICE);

                            dm.enqueue(request);
                        }
                    }
                    break;
            }
            Log.d(TAG, "onDownloadStart: "+mimetype);
            Log.d(TAG, "onDownloadStart: "+url);

        }catch(Exception e){
            Log.d(TAG, "onDownloadStart: "+e.getMessage());
            Toast.makeText(MainActivity.this, "Error in Downloading file !", Toast.LENGTH_SHORT).show();
        }
    }
    // Create an image file
//    private File createImageFile() throws IOException{
//        @SuppressLint("SimpleDateFormat") String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
//        String imageFileName = "img_"+timeStamp+"_";
//        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
//        return File.createTempFile(imageFileName,".jpg",storageDir);
//    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File imageFile = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        return imageFile;
    }
    class MyWebChromeClient extends WebChromeClient {
        @Override
        public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
            super.onGeolocationPermissionsShowPrompt(origin, callback);
            callback.invoke(origin, true, false);
        }

        // For 3.0+ Devices (Start)
        // onActivityResult attached before constructor
        protected void openFileChooser(ValueCallback uploadMsg, String acceptType) {
            mUploadMessage = uploadMsg;
            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
            i.addCategory(Intent.CATEGORY_OPENABLE);
            i.setType("image/*");
            startActivityForResult(Intent.createChooser(i, "File Chooser"), FILECHOOSER_RESULTCODE);
        }


        // For Lollipop 5.0+ Devices
        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
        public boolean onShowFileChooser(WebView mWebView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
            if (mUMA != null) {
                mUMA.onReceiveValue(null);
                mUMA = null;
            }

            mUMA = filePathCallback;

            Intent intent = fileChooserParams.createIntent();
            try {
                startActivityForResult(intent, REQUEST_SELECT_FILE);
            } catch (ActivityNotFoundException e) {
                mUMA = null;
                Toast.makeText(MainActivity.this, "Cannot Open File Chooser", Toast.LENGTH_LONG).show();
                return false;
            }
            return true;
        }

        //For Android 4.1 only
        protected void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
            mUploadMessage = uploadMsg;
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("image/*");
            startActivityForResult(Intent.createChooser(intent, "File Chooser"), FILECHOOSER_RESULTCODE);
        }

        protected void openFileChooser(ValueCallback<Uri> uploadMsg) {
            mUploadMessage = uploadMsg;
            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
            i.addCategory(Intent.CATEGORY_OPENABLE);
            i.setType("image/*");
            startActivityForResult(Intent.createChooser(i, "File Chooser"), FILECHOOSER_RESULTCODE);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(webView!=null){
            if (event.getAction() == KeyEvent.ACTION_DOWN) {
                switch (keyCode) {
                    case KeyEvent.KEYCODE_BACK:
                        if (webView.canGoBack()) {
                            webView.goBack();
                        } else {
                            //finish();
                            onBackPressed();
                        }
                        return true;
                }

            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void error(String message) {
        hideAll();
        onShowCommonErrorPage();
    }

    @Override
    public void navigationDataSuccess(List<NavigationDataModel> mNavigationDataModels) {
         navigationDataModels.addAll(mNavigationDataModels);

         // add logout
         NavigationDataModel navigationDataModel=new NavigationDataModel();
         navigationDataModel.setLogout(true);
         navigationDataModel.setTitle(getString(R.string.logout));
        navigationDataModels.add(navigationDataModel);

         navigationDataAdapter.notifyDataSetChanged();

        //loadWebView();
    }

    @Override
    public void getNavigationItem(NavigationDataModel navigationDataModel) {
        if(navigationDataModel.isLogout()){
            logoutUser();
        }else{
            showLoading();
            currentNavigationDataModel=navigationDataModel;
            webView.loadUrl(navigationDataModel.getUrl());
        }
        closeNavigationDrawer();
    }

    @Override
    public void showWebviewError() {
        LinearLayout mainContent=findViewById(R.id.webview_page_error);
        mainContent.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideWebviewError() {
        LinearLayout mainContent=findViewById(R.id.webview_page_error);
        mainContent.setVisibility(View.GONE);
    }

    @Override
    public void showLoading() {
        LinearLayout mainContent=findViewById(R.id.progress_bar);
        mainContent.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        LinearLayout mainContent=findViewById(R.id.progress_bar);
        mainContent.setVisibility(View.GONE);
    }

    @Override
    public void ifLocationPermissionGranted(boolean val) {
        if(!val){
            // remove location updates
            Intent i = new Intent(MainActivity.this, LocationPermissionActivity.class);
            //startActivity(i);
            startActivityForResult(i, PERMISSION_RESULT);
        }else{
            // get current location
            mPresenter.getLastLocation();
        }
    }

    @Override
    public void ifStoragePermissionGranted(boolean val) {
        if(!val){
            // remove location updates
            Intent i = new Intent(MainActivity.this, StoragePermissionActivity.class);
            //startActivity(i);
            startActivityForResult(i, PERMISSION_RESULT_STORAGE);
        }else{
            // get current location
           // mPresenter.getLastLocation();
        }
    }

    @Override
    public void getCurrentLocation(Location location) {
        Log.d(TAG, "getCurrentLocation: "+location.getLatitude());
        CurrentLocationModel currentLocationModel=new CurrentLocationModel();
        currentLocationModel.setLatitude(location.getLatitude());
        currentLocationModel.setLongitude(location.getLongitude());

        Intent updateServiceIntent = new Intent(this, CurrentLocationUpdate.class);
        Bundle updateBundle = new Bundle();
        updateBundle.putSerializable(GlobalConstant.CURRENT_LOCATION, currentLocationModel);
        updateServiceIntent.putExtras(updateBundle);
        CurrentLocationUpdate.enqueueWork(this, updateServiceIntent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult: "+requestCode);
        if (requestCode == PERMISSION_RESULT) {
            if(resultCode==RESULT_CANCELED){
                
            }else{
                mPresenter.getLastLocation();

            }
        }
        if (requestCode == GpsUtils.REQUEST_CHECK_SETTINGS) {
            switch (resultCode)
            {
                case Activity.RESULT_OK:
                {
                    // All required changes were successfully made
                    // ge current location
                    mPresenter.getLastLocation();
                    break;
                }
                case RESULT_CANCELED:
                {
                    // The user was asked to change settings, but chose not to
                    Log.d(TAG, "onActivityResult: result not ok");
                    break;
                }
                default:
                {
                    break;
                }
            }
        }
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            if (requestCode == REQUEST_SELECT_FILE) {
//                if (mUMA == null)
//                    return;
//                mUMA.onReceiveValue(WebChromeClient.FileChooserParams.parseResult(resultCode, data));
//                mUMA = null;
//            }
//        } if (requestCode == FILECHOOSER_RESULTCODE) {
//            if (null == mUploadMessage)
//                return;
//            // Use MainActivity.RESULT_OK if you're implementing WebView inside Fragment
//            // Use RESULT_OK only if you're implementing WebView inside an Activity
//            Uri result = data == null || resultCode != RESULT_OK ? null : data.getData();
//            mUploadMessage.onReceiveValue(result);
//            mUploadMessage = null;
//        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            if (requestCode != INPUT_FILE_REQUEST_CODE || mFilePathCallback == null) {
                super.onActivityResult(requestCode, resultCode, data);
                return;
            }

            Uri[] results = null;

            // Check that the response is a good one
            if (resultCode == Activity.RESULT_OK) {
                if (data == null) {
                    // If there is not data, then we may have taken a photo
                    if (mCameraPhotoPath != null) {
                        results = new Uri[]{Uri.parse(mCameraPhotoPath)};
                    }
                } else {
                    String dataString = data.getDataString();
                    if (dataString != null) {
                        results = new Uri[]{Uri.parse(dataString)};
                    }
                }
            }

            mFilePathCallback.onReceiveValue(results);
            mFilePathCallback = null;

        } else if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
            if (requestCode != FILECHOOSER_RESULTCODE || mUploadMessage == null) {
                super.onActivityResult(requestCode, resultCode, data);
                return;
            }

            if (requestCode == FILECHOOSER_RESULTCODE) {

                if (null == this.mUploadMessage) {
                    return;

                }

                Uri result = null;

                try {
                    if (resultCode != RESULT_OK) {

                        result = null;

                    } else {

                        // retrieve from the private variable if the intent is null
                        result = data == null ? mCapturedImageURI : data.getData();
                    }
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "activity :" + e,
                            Toast.LENGTH_LONG).show();
                }

                mUploadMessage.onReceiveValue(result);
                mUploadMessage = null;

            }
        }

    }
    @Override
    public void isNetworkConnected(boolean val) {
        Toast.makeText(this, getString(R.string.internet_connection_issue), Toast.LENGTH_LONG).show();
        onShowCommonErrorPage();
    }

    @Override
    public void onShowMainContent() {
        LinearLayout mainContent=findViewById(R.id.main_content);
        mainContent.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHideMainContent() {
        LinearLayout mainContent=findViewById(R.id.main_content);
        mainContent.setVisibility(View.GONE);
    }

    @Override
    public void onShowCommonErrorPage() {
        LinearLayout mainContent=findViewById(R.id.page_error);
        mainContent.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHideCommonErrorPage() {
        LinearLayout mainContent=findViewById(R.id.page_error);
        mainContent.setVisibility(View.GONE);
    }

    @Override
    public void onShowNoData() {

    }

    @Override
    public void onHideNoData() {

    }

    @Override
    public void logoutUser() {
       logout();
    }


    @Override
    public void hideKeyboard() {
        GlobalConstant.hideSoftKeyboard(this);
    }

    @Override
    public void forceLogout(String message) {
        super.forceLogout(message);
    }
    @Override
    public void hideAll() {
      onHideCommonErrorPage();
      onHideMainContent();
      hideWebviewError();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
