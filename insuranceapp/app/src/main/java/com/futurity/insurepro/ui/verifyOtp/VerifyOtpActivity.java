package com.futurity.insurepro.ui.verifyOtp;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.futurity.insurepro.R;
import com.futurity.insurepro.broadcastReceiver.AppSMSBroadcastReceiver;
import com.futurity.insurepro.components.GlobalConstant;
import com.futurity.insurepro.model.LoginModel;
import com.futurity.insurepro.model.LoginResponseModel;
import com.futurity.insurepro.model.SendOtpModel;
import com.futurity.insurepro.model.UserModel;
import com.futurity.insurepro.model.UserProfileModel;
import com.futurity.insurepro.ui.base.BaseActivity;
import com.futurity.insurepro.ui.main.MainActivity;
import com.mukesh.OtpView;


public class VerifyOtpActivity extends BaseActivity implements VerifyOtpView {

    VerifyOtpPresenter<VerifyOtpView> mPresenter;
    private static String TAG="SendOtpactivity";
    SendOtpModel sendOtpModel;
    private OtpView pin;
    private IntentFilter intentFilter;
    private AppSMSBroadcastReceiver appSMSBroadcastReceiver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_otp);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Window w = getWindow();
            w.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            w.setStatusBarColor(getColor(R.color.enter_mobile));
        }

        Intent intent = this.getIntent();
        Bundle bundle = intent.getExtras();

        sendOtpModel = (SendOtpModel) bundle.getSerializable(GlobalConstant.SEND_OTP_MODEL);

        mPresenter=new VerifyOtpPresenter<>(this);
        mPresenter.onAttach(this);
        mPresenter.setSendOtpModel(sendOtpModel);

        pin = (OtpView) findViewById(R.id.otp_view);
        mPresenter.setOtpWatcher(pin);

        initBroadCast();
        smsListener();
    }

    public void onExplore(View view){
        switch(view.getId()){
            case R.id.resend_otp:
                pin.setText("");
                 mPresenter.resendOtp(sendOtpModel);
                break;
        }
    }

    @Override
    public void updateMobileNo(String mobileNo) {
        TextView mobileNoView=findViewById(R.id.mobile_no);
        mobileNoView.setText(mobileNo);
    }

    @Override
    public void getOtpResendSuccess() {
        Toast.makeText(this, getString(R.string.otp_sent), Toast.LENGTH_LONG).show();
    }

    @Override
    public void getOtpResendFailure(String error) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void setTitle() {

    }

    @Override
    public void showLoading() {
        LinearLayout mainContent=findViewById(R.id.progress_bar);
        mainContent.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        LinearLayout mainContent=findViewById(R.id.progress_bar);
        mainContent.setVisibility(View.GONE);
    }

    @Override
    public void isNetworkConnected(boolean val) {
        Toast.makeText(this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onShowMainContent() {
        LinearLayout mainContent=findViewById(R.id.main_content);
        mainContent.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHideMainContent() {
        LinearLayout mainContent=findViewById(R.id.main_content);
        mainContent.setVisibility(View.GONE);
    }

    @Override
    public void onShowCommonErrorPage() {

    }

    @Override
    public void onHideCommonErrorPage() {

    }

    @Override
    public void logoutUser() {

    }

    @Override
    public void hideKeyboard() {
        GlobalConstant.hideSoftKeyboard(this);
    }

    @Override
    public void forceLogout(String message) {
      super.forceLogout(message);
    }

    @Override
    public void onShowNoData() {

    }

    @Override
    public void onHideNoData() {

    }

    @Override
    public void hideAll() {

    }

    @Override
    public void otpMatchSuccess() {
        LoginModel loginModel=new LoginModel();
        loginModel.setUsername(sendOtpModel.getMobile());
        loginModel.setPassword(GlobalConstant.DEFAULT_PASSWORD);
        mPresenter.login(loginModel);
    }

    @Override
    public void error(String message) {
        Toast.makeText(VerifyOtpActivity.this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void loginSuccess(LoginResponseModel loginResponseModel) {
        UserProfileModel userProfileModel=new UserProfileModel();
        userProfileModel.setFirstname(loginResponseModel.getName());
        userProfileModel.setId(loginResponseModel.getUserId());
        userProfileModel.setRedirectUrl(loginResponseModel.getUrl());
        userProfileModel.setProfileImage(loginResponseModel.getProfilePic());
        userProfileModel.setUsername(loginResponseModel.getUsername());
        userProfileModel.setPassword(GlobalConstant.DEFAULT_PASSWORD);

        UserModel.setRid(this,loginResponseModel.getRid());
        UserModel.login(this,userProfileModel);
        Log.d(TAG, "loginSuccess: ");
        Intent i = new Intent(VerifyOtpActivity.this, MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(i);
    }

    @Override
    public void otpMatchFailure() {
        Toast.makeText(VerifyOtpActivity.this, getString(R.string.otp_not_matched), Toast.LENGTH_SHORT).show();
    }

    private void initBroadCast() {
        intentFilter = new IntentFilter("com.google.android.gms.auth.api.phone.SMS_RETRIEVED");
        appSMSBroadcastReceiver = new AppSMSBroadcastReceiver();
        appSMSBroadcastReceiver.setOnSmsReceiveListener(new AppSMSBroadcastReceiver.OnSmsReceiveListener() {
            @Override
            public void onReceive(String code) {
                pin.setText(code);
                //Toast.makeText(VerifyOtpActivity.this, code, Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void smsListener() {
        SmsRetrieverClient client = SmsRetriever.getClient(this);
        client.startSmsRetriever();
    }



    @Override
    public void onResume() {
        super.onResume();
        registerReceiver(appSMSBroadcastReceiver, intentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(appSMSBroadcastReceiver);
    }
}
