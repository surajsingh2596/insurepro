
package com.futurity.insurepro.ui.verifyOtp;

import com.futurity.insurepro.model.LoginModel;
import com.futurity.insurepro.model.SendOtpModel;
import com.futurity.insurepro.ui.base.MvpPresenter;
import com.mukesh.OtpView;

public interface VerifyOtpMvpPresenter<V extends VerifyOtpView> extends MvpPresenter<V> {
    void updateView();
    void setOtpWatcher(OtpView otpView);
    void resendOtp(SendOtpModel sendOtpModel);
    void setSendOtpModel(SendOtpModel sendOtpModel);
    void verifyOTP(String pin);
    void login(LoginModel loginModel);

}
