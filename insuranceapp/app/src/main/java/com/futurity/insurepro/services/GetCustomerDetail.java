package com.futurity.insurepro.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;

import com.futurity.insurepro.R;
import com.futurity.insurepro.components.GlobalConstant;
import com.futurity.insurepro.model.CommonResponseModel;
import com.futurity.insurepro.model.CustomerDetailModel;
import com.futurity.insurepro.model.GetCustomerDetailRequestModel;
import com.futurity.insurepro.model.UserModel;
import com.futurity.insurepro.network.ApiClientInterface;
import com.futurity.insurepro.network.NewApiClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetCustomerDetail extends IntentService
{
    private boolean ifSuccess;
    public static final String TAG = GetCustomerDetail.class.getSimpleName();
    public GetCustomerDetail()
    {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent)
    {
        try{
            String mobileNo=intent.getStringExtra(GlobalConstant.MOBILE_NO);

            GetCustomerDetailRequestModel getCustomerDetailRequestModel=new GetCustomerDetailRequestModel();
            getCustomerDetailRequestModel.setMobile(mobileNo);
            getCustomerDetailRequestModel.setRid(UserModel.getRid(getApplicationContext()));

            ApiClientInterface apiClientInterface= NewApiClient.getApiClient().create(ApiClientInterface.class);
            apiClientInterface.getCustomerDetail(getCustomerDetailRequestModel).enqueue(new Callback<CommonResponseModel<CustomerDetailModel>>()
            {
                @Override
                public void onResponse(Call<CommonResponseModel<CustomerDetailModel>> call, Response<CommonResponseModel<CustomerDetailModel>> response)
                {
                    if(response.isSuccessful()){
                        try{
                            CommonResponseModel<CustomerDetailModel> responseModel=response.body();
                            switch(responseModel.getStatus()){
                                case GlobalConstant.RESPONSE_SUCCESS:
                                    ifSuccess=true;
                                    Log.d(TAG, "onResponse: "+responseModel.getData().getName());
                                    onLayout(getApplicationContext());
                                    break;
                                case 0:
                                default:
                                    ifSuccess=false;
                                    break;
                            }
                        }catch (Exception e){
                            ifSuccess=false;
                        }

                    }else{
                        ifSuccess=false;
                    }
                }
                @Override
                public void onFailure(Call<CommonResponseModel<CustomerDetailModel>> call, Throwable t)
                {
                    call.cancel();
                    ifSuccess=false;
                }
            });
        }catch (Exception e){

        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(ifSuccess){
            //onLayout(getApplicationContext());
        }
    }
    public void onLayout(Context context) {

        Log.d(TAG, "onLayout: "+GlobalConstant.VIEW_ADDED);
        GlobalConstant.VIEW_ADDED=true;
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.TYPE_ACCESSIBILITY_OVERLAY |
                WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY,
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL |
                        WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSPARENT);

        params.height = WindowManager.LayoutParams.MATCH_PARENT;
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.format = PixelFormat.TRANSLUCENT;

        params.gravity = Gravity.TOP;

        LinearLayout view = new LinearLayout(context);
        view.setId(R.id.accelerate);
        view.setBackgroundColor(R.color.transparent);
        view.setOrientation(LinearLayout.VERTICAL);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );

        View hiddenInfo = inflater.inflate(R.layout.popup_after_call, view, false);
        Button button=hiddenInfo.findViewById(R.id.btSignIn);
        button.setOnClickListener(v->{
            wm.removeView(view);
        });
        view.addView(hiddenInfo);

        wm.addView(view, params);
    }
}
