
package com.futurity.insurepro.ui.login;


import com.futurity.insurepro.model.LoginModel;
import com.futurity.insurepro.ui.base.MvpPresenter;

public interface LoginMvpPresenter<V extends LoginView> extends MvpPresenter<V> {
    void login(LoginModel loginModel);
}
