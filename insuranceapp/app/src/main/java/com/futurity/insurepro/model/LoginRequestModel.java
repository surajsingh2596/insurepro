package com.futurity.insurepro.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LoginRequestModel extends CommonLoginRequestModel implements Serializable {

    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("firstpage")
    @Expose
    private String firstpage;

    @SerializedName("rid")
    @Expose
    private int rid;

    public LoginRequestModel() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getfpage() {
        return firstpage;
    }

    public void setfpage(String firstpage) {
        this.firstpage = firstpage;
    }

    public int getRid() {
        return rid;
    }

    public void setRid(int rid) {
        this.rid = rid;
    }
}
