
package com.futurity.insurepro.ui.splash;


import com.futurity.insurepro.ui.base.MvpPresenter;

public interface SplashMvpPresenter<V extends SplashView> extends MvpPresenter<V> {
    void checkIfUserLogin();
    void login();
}
