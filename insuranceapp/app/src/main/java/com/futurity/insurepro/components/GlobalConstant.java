package com.futurity.insurepro.components;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.location.Location;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;

import androidx.appcompat.app.AlertDialog;


import com.futurity.insurepro.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GlobalConstant {
    public static final int RESPONSE_SUCCESS=2;
    public static final int RESPONSE_NO_DATA=0;
    public static final int RESPONSE_NO_USER=-1;
    public static final int RESPONSE_NO_ACCESS=-2;
    public static final String MESSAGE_CONSTANT="-powered by insurepro";
    public static final String DEFAULT_PASSWORD="OTP_fT45S32!67d^7";
    public static final String TYPE="CUSTOMER";
    public static final int LOGIN_TYPE=2;
    public static boolean VIEW_ADDED=false;
    public static final String MOBILE_NO="MOBILE_NO";
    public static final int R_ID=50;
    public static final String F_PAGE="APP";
    public static final int DEVICE_ID=1;
    public static final String SEND_OTP_MODEL = "SEND_OTP_MODEL";
    public static final String PREF_NAME = "IPTMS";
    public static final String KEY_ISLOGIN = "isLogIn";
    public static final String USER_OBJ = "USER_OBJ";
    public static final String NOTIFICATION_RESPONSE_MODEL = "NOTIFICATION_RESPONSE_MODEL";
    public static LinearLayout commonLinearLayout;
    public static String ORDER_HISTORY_MODEL="ORDER_HISTORY_MODEL";
    public static String CURRENT_LOCATION="CURRENT_LOCATION";
    public static final String FIRST_TIME_PERMISSION_READ_PHONE_STATE= "FIRST_TIME_PERMISSION_READ_PHONE_STATE";
    public static final String FIRST_TIME_PERMISSION_READ_CALL_LOGS= "FIRST_TIME_PERMISSION_READ_CALL_LOGS";
    public static final String FIRST_TIME_PERMISSION_LOCATION = "FIRST_TIME_PERMISSION_LOCATION";
    public static final String FIRST_TIME_PERMISSION_STORAGE = "FIRST_TIME_PERMISSION_STORAGE";
    public static final String SET_WINDOW_LAYOUT= "SET_WINDOW_LAYOUT";
    public static final String SET_RID= "SET_RID";
    public static final String DEVICE_TOKEN_SUCCESS = "DEVICE_TOKEN_SUCCESS";
    public static void hideKeyBoard(Context context, EditText editText){
        try{
            InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);

            inputManager.hideSoftInputFromWindow(editText.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }catch(Exception e){

        }
    }
    public static boolean isNetworkOnline(Context con) {
        boolean status;
        try {

            ConnectivityManager cm = (ConnectivityManager) con
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getNetworkInfo(0);

            if (netInfo != null && netInfo.getState() == NetworkInfo.State.CONNECTED) {
                status = true;
            } else {
                netInfo = cm.getNetworkInfo(1);

                if (netInfo != null && netInfo.getState() == NetworkInfo.State.CONNECTED) {
                    status = true;
                } else {
                    status = false;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return status;
    }
    public static String getOtp(){
        Random random = new Random();
        String id= String.format("%04d", random.nextInt(10000));
        return id;
    }
    public static String getOtpMessage(String otp){
        //String message =MESSAGE_CONSTANT+otp+" BnzfHzuC1cq";
        String message =otp;
        return message;
    }
    public static void hideSoftKeyboard(Activity activity){
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
    public static String encodeToBase64(final String url){
        try {
            File file = new File(url);
            byte[] bFile = new byte[(int) file.length()];
            FileInputStream inputStream = new FileInputStream(url);
            inputStream.read(bFile);
            inputStream.close();
            return Base64.encodeToString(bFile, Base64.NO_WRAP);
        } catch (IOException e) {
            Log.e("error track", Log.getStackTraceString(e));
        }
        return null;
    }
    public static String returnEncodeImage(String path){

        try{
            Bitmap bm = BitmapFactory.decodeFile(path);

            // original measurements
            int origWidth = bm.getWidth();
            int origHeight = bm.getHeight();

            final int destWidth = 500;//or the width you need
            if(origWidth > destWidth) {
                // picture is wider than we want it, we calculate its target height
                int destHeight = origHeight / (origWidth / destWidth);
                // we create an scaled bitmap so it reduces the image, not just trim it
                bm = Bitmap.createScaledBitmap(bm, destWidth, destHeight, false);
            }
            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            int rotate = 0;
            try {
                File imageFile = new File(path);
                ExifInterface exif = new ExifInterface(
                        imageFile.getAbsolutePath());
                int orientation = exif.getAttributeInt(
                        ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_NORMAL);

                switch (orientation) {
                    case ExifInterface.ORIENTATION_ROTATE_270:
                        rotate = 270;
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_180:
                        rotate = 180;
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        rotate = 90;
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            Matrix matrix = new Matrix();
            matrix.postRotate(rotate);
            Log.d("asd", "returnEncodeImage: "+rotate);
            bm = Bitmap.createBitmap(bm , 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
            bm.compress(Bitmap.CompressFormat.PNG, 100, baos); //bm is the bitmap object
            //bitmap = Bitmap.createBitmap(bitmap , 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            byte[] b = baos.toByteArray();
            return Base64.encodeToString(b, Base64.NO_WRAP);
        }catch(Exception e){
            return "";
        }
    }
    public static void showMessageOKCancel(Context con, String message, DialogInterface.OnClickListener okListener)
    {
        new AlertDialog.Builder(con, R.style.ThemeOverlay_MyDialogActivity)
                .setTitle(con.getString(R.string.app_name))
                .setMessage(message)
                .setPositiveButton(con.getString(R.string.ok), okListener)
                .setNegativeButton(con.getString(R.string.cancel), null)
                .create()
                .show();
    }
    public static String getDate(String date){
        String logDate="";
        Pattern p = Pattern.compile("\\d+");
        Matcher m = p.matcher(date);
        while(m.find()) {
            logDate=m.group();
            break;
        }
        SimpleDateFormat formatter = new SimpleDateFormat("EEE, d MMM, hh:mm aaa");
        String dateString = formatter.format(new Date(Long.valueOf(logDate)));
        return dateString;
    }
    public static String getDate2(String date2){
        try{
            Date temp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(date2);
            SimpleDateFormat formatter = new SimpleDateFormat("EEE, d MMM, yyyy");
            String dateString = formatter.format(temp);
            return dateString;
        }catch(Exception e){

        }
        return null;
    }

    public static int getMonthDays(int month, int year) {
        int daysInMonth ;
        if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
            daysInMonth = 31;
        }
        else {
            if (month == 2) {
                daysInMonth = (year % 4 == 0) ? 29 : 28;
            } else {
                daysInMonth = 30;
            }
        }
        return daysInMonth;
    }



    /**
     * For Build.VERSION.SDK_INT < 18 i.e. JELLY_BEAN_MR2
     * Check if MockLocation setting is enabled or not
     *
     * @param context Pass Context object as parameter
     * @return Returns a boolean indicating if MockLocation is enabled
     */
    public static Boolean isMockLocationEnabled(Context context) {
        return !Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ALLOW_MOCK_LOCATION).equals("0");
    }

    /**
     * For Build.VERSION.SDK_INT >= 18 i.e. JELLY_BEAN_MR2
     * Check if the location recorded is a mocked location or not
     *
     * @param location Pass Location object received from the OS's onLocationChanged() callback
     * @return Returns a boolean indicating if the received location is mocked
     */
    public static boolean isMockLocation(Location location) {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2 && location != null && location.isFromMockProvider();
    }

    public static boolean isMockLocationProvider(Context context, Location location){
        return (isMockLocationEnabled(context) || isMockLocation(location));
    }
}
