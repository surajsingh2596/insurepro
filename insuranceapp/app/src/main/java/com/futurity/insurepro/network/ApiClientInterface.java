package com.futurity.insurepro.network;

import com.futurity.insurepro.model.CommonResponseModel;
import com.futurity.insurepro.model.CustomerDetailModel;
import com.futurity.insurepro.model.GetCustomerDetailRequestModel;
import com.futurity.insurepro.model.LoginRequestModel;
import com.futurity.insurepro.model.LoginResponseModel;
import com.futurity.insurepro.model.MenuNavigatorRequestModel;
import com.futurity.insurepro.model.NavigationDataModel;
import com.futurity.insurepro.model.OtpMessageResponse;
import com.futurity.insurepro.model.SendOtpRequestModel;
import com.futurity.insurepro.model.UpdateDeviceTokenRequestModel;
import com.futurity.insurepro.model.UpdateGeoLocationRequestModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;


public interface ApiClientInterface {

    @Headers("Content-type: application/json")
    @GET("CheckCredential")
    Call<CommonResponseModel<LoginResponseModel>> loginUser(@Query("username") String username, @Query("password") String password, @Query("type") String type, @Query("rid") int rid);

    @Headers("Content-type: application/json")
    @POST("Login")
    Call<CommonResponseModel<LoginResponseModel>> loginUser(@Body LoginRequestModel loginRequestModel);

    @Headers("Content-type: application/json")
    @POST("mobile_navigator_menu")
    Call<CommonResponseModel<List<NavigationDataModel>>> navigationData(@Body MenuNavigatorRequestModel menuNavigatorRequestModel);

    @Headers("Content-type: application/json")
    @GET("SendSMS")
    Call<OtpMessageResponse> sendOtp(@Query("APIKey") String apiKey,
                                     @Query("senderid") String senderId,
                                     @Query("DCS") int dcs,
                                     @Query("channel") int channel,
                                     @Query("flashsms") int flashSms,
                                     @Query("number") String  mobile,
                                     @Query("text") String  text,
                                     @Query("route") int route);

    @Headers("Content-type: application/json")
    @POST("customerdetailsbynumber")
    Call<CommonResponseModel<CustomerDetailModel>> getCustomerDetail(@Body GetCustomerDetailRequestModel getCustomerDetailRequestModel);

    @Headers("Content-type: application/json")
    @POST("updateDeviceID")
    Call<CommonResponseModel<String>> updateDeviceToken(@Body UpdateDeviceTokenRequestModel updateDeviceTokenRequestModel);

    @Headers("Content-type: application/json")
    @POST("SendMSG")
    Call<CommonResponseModel<String>> sendOtp(@Body SendOtpRequestModel sendOtpRequestModel);

    @Headers("Content-type: application/json")
    @POST("updateGeoLocation")
    Call<CommonResponseModel<String>> updateGeoLocation(@Body UpdateGeoLocationRequestModel updateGeoLocationRequestModel);
}