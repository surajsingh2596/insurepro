package com.futurity.insurepro.ui.login;


import com.futurity.insurepro.model.LoginResponseModel;
import com.futurity.insurepro.ui.base.BaseView;

public interface LoginView extends BaseView {

    boolean validate();
    void error(String message);
    void loginSuccess(LoginResponseModel loginResponseModel);
    boolean validateTerm();
}
