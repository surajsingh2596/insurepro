
package com.futurity.insurepro.ui.permission;

import com.futurity.insurepro.model.PermissionModel;
import com.futurity.insurepro.ui.base.MvpPresenter;

import java.util.List;

public interface PermissionMvpPresenter<V extends PermissionView> extends MvpPresenter<V> {
     void setPermissionModelList(List<PermissionModel> permissionModelList);
     void updateButtons();
     void checkPermissions();
     void checkOverlay();
     void drawOverlay(boolean val);
     boolean getAllPermission();
     void checkAllPermit();
}
