package com.futurity.insurepro.ui.base;


public interface BaseView {
    void setTitle();
    void showLoading();
    void hideLoading();
    void isNetworkConnected(boolean val);
    void onShowMainContent();
    void onHideMainContent();
    void onShowCommonErrorPage();
    void onHideCommonErrorPage();
    void onShowNoData();
    void onHideNoData();
    void logoutUser();
    void hideKeyboard();
    void forceLogout(String message);
    void hideAll();
}
