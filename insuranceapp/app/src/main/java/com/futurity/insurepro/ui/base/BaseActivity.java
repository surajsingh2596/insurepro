package com.futurity.insurepro.ui.base;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.futurity.insurepro.R;
import com.futurity.insurepro.components.GlobalConstant;
import com.futurity.insurepro.model.UserModel;
import com.futurity.insurepro.services.SendTokenToServerService;
import com.futurity.insurepro.ui.login.LoginActivity;
import com.futurity.insurepro.ui.permission.MarshMallowPermission;
import com.futurity.insurepro.ui.permission.PermissionActivity;
import com.futurity.insurepro.ui.sendOtp.SendOtpActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;


public class BaseActivity extends AppCompatActivity {
    private static String TAG="Base activity";
    private long lastPress;
    protected boolean ifDoubleBack=false;
    protected boolean ifNotCheckPermission;
    private static final int MY_REQUEST_CODE=189;
    protected LinearLayout loader, dataLoading,dataReload,mainContent;
    protected boolean updateDeviceToken=true;
    protected LinearLayout progressBar;

    public void backButton(View view){
        onBackPressed();
    }
    final int PERMISSION_RESULT = 375;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkIfDeviceTokenUpdated();
    }

    private void checkIfDeviceTokenUpdated(){
        if(!updateDeviceToken)
            return;
        try{
            FirebaseInstanceId.getInstance().getInstanceId()
                    .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                        @Override
                        public void onComplete(@NonNull com.google.android.gms.tasks.Task<InstanceIdResult> task) {
                            if (!task.isSuccessful()) {
                                Log.w(TAG, "getInstanceId failed", task.getException());
                                return;
                            }
                            Log.d(TAG, "onComplete: ssss");
                            // Get new Instance ID token
                            String token = task.getResult().getToken();
                            String oldToken=UserModel.getDeviceTokeSendSuccess(BaseActivity.this);
                            if(!oldToken.equals("") && oldToken.equals(token))
                                return;
                            Log.d(TAG, "onComplete: ssssssssssssss");
                            Intent intent = new Intent(BaseActivity.this, SendTokenToServerService.class);
                            startService(intent);
                        }
                    });
        }catch(Exception e){

        }

    }

    @Override
    public void onBackPressed()
    {
        if(ifDoubleBack){
            long currentTime = System.currentTimeMillis();
            if (currentTime - lastPress > 5000)
            {
                Toast.makeText(this, getString(R.string.press_again),
                        Toast.LENGTH_LONG).show();
                lastPress = currentTime;
            }
            else
            {
                super.onBackPressed();
            }
        }else{
            super.onBackPressed();
        }

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here

                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public void onResume() {
        super.onResume();
        if(!ifNotCheckPermission){
           // checkForAppPermission();
        }

    }
    public void logout(){
        GlobalConstant.showMessageOKCancel(this, getString(R.string.want_logout), new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                Class<?> aClass=null;
                if(GlobalConstant.LOGIN_TYPE==1){
                    aClass=LoginActivity.class;
                }else if(GlobalConstant.LOGIN_TYPE==2){
                    aClass= SendOtpActivity.class;
                }

                if(aClass!=null){
                    UserModel.logout(BaseActivity.this);
                    startActivity(new Intent(BaseActivity.this, aClass));
                    finish();
                }

            }
        });
    }

    public void forceLogout(String message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        Class<?> aClass=null;
        if(GlobalConstant.LOGIN_TYPE==1){
            aClass=LoginActivity.class;
        }else if(GlobalConstant.LOGIN_TYPE==2){
            aClass= SendOtpActivity.class;
        }
        UserModel.logout(this);
        Intent i = new Intent(BaseActivity.this, aClass);
        startActivity(i);
        finish();
    }
    public void logoutUser() {
//        Toast.makeText(this,getResources().getString(R.string.login_again), Toast.LENGTH_SHORT).show();
//        Intent i = new Intent(BaseActivity.this, LoginActivity.class);
//        startActivity(i);
//        finish();
    }
    public void checkForAppPermission() {
        MarshMallowPermission marshMallowPermission=new MarshMallowPermission(this);
        if(!marshMallowPermission.checkPermissionForReadPhoneState() || !marshMallowPermission.checkPermissionForReadCallLog()){
            Intent i = new Intent(BaseActivity.this, PermissionActivity.class);
            //startActivity(i);
            startActivityForResult(i, PERMISSION_RESULT);
        }else{
            Log.d(TAG, "checkForAppPermission: 23");
        }
    }
    private boolean canDrawOverApp(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
           return Settings.canDrawOverlays(this);
        }
        return true;
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PERMISSION_RESULT) {
            if(resultCode==RESULT_CANCELED){
                logout();
               // Toast.makeText(getContext(), getString(R.string.location_p), Toast.LENGTH_SHORT).show();
            }else{
                Log.d(TAG, "onActivityResult: ");
            }
        }

    }
}
