package com.futurity.insurepro.ui.sendOtp;

import android.content.Context;


import com.futurity.insurepro.R;
import com.futurity.insurepro.components.GlobalConstant;
import com.futurity.insurepro.model.CommonResponseModel;
import com.futurity.insurepro.model.SendOtpModel;
import com.futurity.insurepro.model.SendOtpRequestModel;
import com.futurity.insurepro.model.UserModel;
import com.futurity.insurepro.network.ApiClientInterface;
import com.futurity.insurepro.network.NewApiClient;
import com.futurity.insurepro.ui.base.BasePresenter;

import java.io.IOException;
import java.security.KeyStore;

import javax.crypto.Cipher;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SendOtpPresenter<V extends SendOtpView> extends BasePresenter<V>
        implements SendOtpMvpPresenter<V> {
    private static String TAG="SendOtpPresenter";
    private Context mContext;

    private KeyStore keyStore;
    // Variable used for storing the key in the Android Keystore container
    private static final String KEY_NAME = "Expressway Mitra";
    private Cipher cipher;
    private SendOtpActivity mSendOtpActivity;

    public SendOtpPresenter(SendOtpActivity sendOtpActivity) {
        mContext=(Context)sendOtpActivity;
        mSendOtpActivity=sendOtpActivity;
    }

    @Override
    public void onAttach(V baseView) {
        super.onAttach(baseView);
        onStart();
        setRidVal();
    }
    private void onStart(){

    }
    private void setRidVal(){
        UserModel.setRid(mContext,GlobalConstant.R_ID);
    }
    @Override
    public void sendOtp(SendOtpModel sendOtpModel) {
        sendOtpModel.setOtp(GlobalConstant.getOtp());
        sendOtpModel.setText(GlobalConstant.getOtpMessage(sendOtpModel.getOtp()));
        getMvpView().hideKeyboard();
        getMvpView().showLoading();

        if (!GlobalConstant.isNetworkOnline(mContext)) {
            getMvpView().isNetworkConnected(false);
            getMvpView().hideLoading();
            return;
        }

        SendOtpRequestModel sendOtpRequestModel=new SendOtpRequestModel();
        sendOtpRequestModel.setFlag("Android");
        sendOtpRequestModel.setUsername(sendOtpModel.getMobile());
        sendOtpRequestModel.setMsg(sendOtpModel.getText());
        sendOtpRequestModel.setRid(UserModel.getRid(mContext));
        sendOtpRequestModel.setType(GlobalConstant.TYPE);
        ApiClientInterface apiClientInterface= NewApiClient.getApiClient().create(ApiClientInterface.class);

        apiClientInterface.sendOtp(sendOtpRequestModel).enqueue(new Callback<CommonResponseModel<String>>()
        {
            @Override
            public void onResponse(Call<CommonResponseModel<String>> call, Response<CommonResponseModel<String>> response)
            {
                if(response.isSuccessful()){
                    getMvpView().hideLoading();
                    CommonResponseModel<String> otpResponseModel=response.body();
                    getMvpView().getSuccess(sendOtpModel);

                }else{
                    getMvpView().hideLoading();
                    getMvpView().getFailure(mContext.getResources().getString(R.string.server_error));
                }

            }

            @Override
            public void onFailure(Call<CommonResponseModel<String>> call, Throwable t)
            {
                call.cancel();
                if (t instanceof IOException) {
                    getMvpView().getFailure(mContext.getResources().getString(R.string.no_internet));
                }
                else {
                    getMvpView().getFailure(mContext.getResources().getString(R.string.server_error));
                }
                getMvpView().hideLoading();
            }
        });
    }


}
