package com.futurity.insurepro.broadcastReceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.futurity.insurepro.R;
import com.futurity.insurepro.components.GlobalConstant;
import com.futurity.insurepro.model.CommonResponseModel;
import com.futurity.insurepro.model.CustomerDetailModel;
import com.futurity.insurepro.model.GetCustomerDetailRequestModel;
import com.futurity.insurepro.model.UserModel;
import com.futurity.insurepro.network.ApiClientInterface;
import com.futurity.insurepro.network.NewApiClient;
import com.futurity.insurepro.services.GetCustomerDetail;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PhoneStateReceiver extends BroadcastReceiver {
    private static final String TAG="PhoneStateReceiver";
    @Override
    public void onReceive(Context context, Intent intent) {
        if(!UserModel.isLogin(context))
            return;
        //Log.d("SDfsd", "onReceive: reee");
        //onLayout(context);
        //getCustomerDetail(context,"");
        //Toast.makeText(context, "Intent Detected.", Toast.LENGTH_LONG).show();
        // This is where you start your service
        try {
            System.out.println("Receiver start");
            String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
            String incomingNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
            if(incomingNumber.startsWith("+91")){
                incomingNumber=incomingNumber.replace("+91","");
            }
            if(incomingNumber.startsWith("0")){
                incomingNumber=incomingNumber.replaceFirst("0","");
            }
            Log.d(TAG, "onReceive: no "+incomingNumber);
            if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
                //Toast.makeText(context, "Incoming Call State", Toast.LENGTH_SHORT).show();
                //Toast.makeText(context, "Ringing State Number is -" + incomingNumber, Toast.LENGTH_SHORT).show();
                getCustomerDetail(context,incomingNumber);
            }
            if ((state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK))) {
                //Toast.makeText(context, "Call Received State", Toast.LENGTH_SHORT).show();
            }
            if (state.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
                //Toast.makeText(context, "Call Idle State", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Log.d("Sdsds", "onReceive: "+e.getMessage());
            e.printStackTrace();
        }

    }
    private void startService(Context context,String mobileNo){
        Intent intent = new Intent(context, GetCustomerDetail.class);
        intent.putExtra(GlobalConstant.MOBILE_NO,mobileNo);
        context.startService(intent);
    }
    private void getCustomerDetail(Context context, String mobile){
        try{
           // String mobileNo=intent.getStringExtra(GlobalConstant.MOBILE_NO);

            GetCustomerDetailRequestModel getCustomerDetailRequestModel=new GetCustomerDetailRequestModel();
            getCustomerDetailRequestModel.setMobile(mobile);
            getCustomerDetailRequestModel.setRid(UserModel.getRid(context));

            ApiClientInterface apiClientInterface= NewApiClient.getApiClient().create(ApiClientInterface.class);
            apiClientInterface.getCustomerDetail(getCustomerDetailRequestModel).enqueue(new Callback<CommonResponseModel<CustomerDetailModel>>()
            {
                @Override
                public void onResponse(Call<CommonResponseModel<CustomerDetailModel>> call, Response<CommonResponseModel<CustomerDetailModel>> response)
                {
                    if(response.isSuccessful()){
                        try{
                            CommonResponseModel<CustomerDetailModel> responseModel=response.body();
                            switch(responseModel.getStatus()){
                                case GlobalConstant.RESPONSE_SUCCESS:
                                    Toast.makeText(context, responseModel.getData().getName(), Toast.LENGTH_SHORT).show();
                                    Log.d(TAG, "onResponse: "+responseModel.getData().getName());
                                    String name="";
                                    String address="";
                                    if(responseModel.getData().getName()!=null){
                                        name=  responseModel.getData().getName();
                                    }
                                    if(responseModel.getData().getAddress()!=null){
                                        address=  responseModel.getData().getAddress();
                                    }
                                    onLayout(context,name,address);
                                    break;
                                case 0:
                                default:
                                    break;
                            }
                        }catch (Exception e){

                        }

                    }else{

                    }
                }
                @Override
                public void onFailure(Call<CommonResponseModel<CustomerDetailModel>> call, Throwable t)
                {
                    call.cancel();

                }
            });
        }catch (Exception e){

        }
    }
    public void onLayout(Context context,String name, String address) {

        Log.d(TAG, "onLayout: "+GlobalConstant.VIEW_ADDED);
        GlobalConstant.VIEW_ADDED=true;
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.TYPE_ACCESSIBILITY_OVERLAY |
                WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY,
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL |
                        WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSPARENT);

        params.height = WindowManager.LayoutParams.MATCH_PARENT;
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.format = PixelFormat.TRANSLUCENT;

        params.gravity = Gravity.TOP;
        Log.d(TAG, "onLayout: dfr "+UserModel.getWindowLayout(context));
        LinearLayout view = new LinearLayout(context);
        //view.setBackgroundColor(context.getResources().getColor(R.color.transparent3));
        view.setOrientation(LinearLayout.VERTICAL);
        view.setOnClickListener(v->{
           // UserModel.setWindowLayout(context,false);
            wm.removeView(view);
        });

        LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );

        View hiddenInfo = inflater.inflate(R.layout.popup_after_call, view, false);
        TextView nameView=hiddenInfo.findViewById(R.id.name);
        TextView addressView=hiddenInfo.findViewById(R.id.address);

        // set data
        nameView.setText(name);
        addressView.setText(address);
        LinearLayout button=hiddenInfo.findViewById(R.id.cancel);
        button.setOnClickListener(v->{
            //UserModel.setWindowLayout(context,false);
            wm.removeView(view);
        });
        view.addView(hiddenInfo);

        wm.addView(view, params);

//        if(!UserModel.getWindowLayout(context)){
//            UserModel.setWindowLayout(context,true);
//
//            LinearLayout view = new LinearLayout(context);
//            //view.setBackgroundColor(context.getResources().getColor(R.color.transparent3));
//            view.setOrientation(LinearLayout.VERTICAL);
//            view.setOnClickListener(v->{
//                UserModel.setWindowLayout(context,false);
//                wm.removeView(view);
//            });
//
//            LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
//
//            View hiddenInfo = inflater.inflate(R.layout.popup_after_call, view, false);
//            TextView nameView=hiddenInfo.findViewById(R.id.name);
//            TextView addressView=hiddenInfo.findViewById(R.id.address);
//
//            // set data
//            nameView.setText(name);
//            addressView.setText(address);
//            LinearLayout button=hiddenInfo.findViewById(R.id.cancel);
//            button.setOnClickListener(v->{
//                UserModel.setWindowLayout(context,false);
//                wm.removeView(view);
//            });
//            view.addView(hiddenInfo);
//
//            wm.addView(view, params);
//        }

    }

}
