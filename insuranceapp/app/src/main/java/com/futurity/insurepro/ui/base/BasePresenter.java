package com.futurity.insurepro.ui.base;

import android.app.Activity;
import android.content.Context;

import androidx.fragment.app.Fragment;


public class BasePresenter<V extends BaseView> implements MvpPresenter<V> {
    private static String TAG="my data";
    private Context mContext;
    private V mBaseView;
    public BasePresenter(){

    }
    @Override
    public void onAttach(V mvpView) {
        mBaseView = mvpView;
        mContext=mvpView instanceof Activity? (Context)mvpView:((Fragment)mvpView).getContext();
        getMvpView().setTitle();
    }
    public V getMvpView() {
        return mBaseView;
    }
    protected void logoutUser() {
        //UserModel.logout(mContext);
        mBaseView.logoutUser();
    }


}

