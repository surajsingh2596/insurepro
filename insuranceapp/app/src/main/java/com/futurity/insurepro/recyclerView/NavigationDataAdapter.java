package com.futurity.insurepro.recyclerView;
/**
 * Created by OWNER on 17-11-2016.
 */

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.futurity.insurepro.R;
import com.futurity.insurepro.model.NavigationDataModel;
import com.futurity.insurepro.ui.main.MainActivity;

import java.util.List;


public class NavigationDataAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {

    private List<NavigationDataModel> navigationDataModels;
    private static final int CHOOSE=1;

    private boolean ifExitPoint=false;
    private Context context;
    private static String TAG="Order adapter";
    private MainActivity mainActivity;
    @Override
    public int getItemViewType(int position) {
        final NavigationDataModel navigationDataModel=navigationDataModels.get(position);
       return CHOOSE;


    }
    public NavigationDataAdapter(List<NavigationDataModel> mNavigationDataModels, Context mContext, MainActivity mainActivity) {
        this.navigationDataModels = mNavigationDataModels;
        this.context=mContext;
        this.mainActivity=mainActivity;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        switch (viewType) {
            case CHOOSE:
                Log.d("viewer","viewer sender 1");
                View viewAd = inflater.inflate(R.layout.navigation_item, viewGroup, false);
                viewHolder= new ViewHolderManage(viewAd);
                break;
            default:
                Log.d("viewer","viewer receiver ");
                View viewAdO = inflater.inflate(R.layout.navigation_item, viewGroup, false);
                viewHolder=new ViewHolderManage(viewAdO);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

        switch (viewHolder.getItemViewType()){
            case CHOOSE:
                ViewHolderManage viewHolderChoose=(ViewHolderManage)viewHolder;
                configureChoose(viewHolderChoose,position);
                break;
        }


    }
    private void configureChoose(final ViewHolderManage viewHolderManage, final int position) {
        Log.d(TAG, "configureChoose: ");
        final NavigationDataModel navigationDataModel=navigationDataModels.get(position);

        TextView siteName=viewHolderManage.getView().findViewById(R.id.title);
        siteName.setText(navigationDataModel.getTitle());

        LinearLayout mainView=viewHolderManage.getView().findViewById(R.id.card_view);
        mainView.setOnClickListener(v->{
            mainActivity.getNavigationItem(navigationDataModel);
        });

    }
    public class ViewHolderManage extends RecyclerView.ViewHolder {

        private View finalView;
        private RelativeLayout imageClick;
        public ViewHolderManage(View itemView) {
            super(itemView);
            finalView=itemView;

        }

        public View getView(){
            return finalView;
        }

    }

    @Override
    public int getItemCount(){
        return navigationDataModels.size();
    }

}

