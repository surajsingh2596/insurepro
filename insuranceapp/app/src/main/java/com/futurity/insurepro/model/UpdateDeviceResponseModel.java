package com.futurity.insurepro.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UpdateDeviceResponseModel implements Serializable {
    @SerializedName("Name")
    @Expose
    private String name;

    @SerializedName("Address")
    @Expose
    private String address;
    public UpdateDeviceResponseModel() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
