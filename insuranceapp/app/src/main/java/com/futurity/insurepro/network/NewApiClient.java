package com.futurity.insurepro.network;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by techelogy2 on 7/1/18.
 */

public class NewApiClient
{
    private static final String SERVER_CENTRAL_IP="https://mycornershop.co.in/api/crt/";
    private static final String SERVER_CENTRAL_IP_OTP="https://www.smsgatewayhub.com/api/mt/";


    private static Retrofit retrofit;


    private NewApiClient()
    {

    }

    public static Retrofit getApiClient()
    {
        createInstance();
        return retrofit;
    }
    public static Retrofit getApiClientOtp()
    {
        createInstance2();
        return retrofit;
    }

    private static void createInstance()
    {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        // set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        httpClient.connectTimeout(1, TimeUnit.MINUTES)
                .writeTimeout(1, TimeUnit.MINUTES)
                .readTimeout(1, TimeUnit.MINUTES);

        httpClient.addInterceptor(logging);

        Retrofit.Builder builder =
                new Retrofit.Builder()
                        .baseUrl(SERVER_CENTRAL_IP)
                        .addConverterFactory(GsonConverterFactory.create());
        retrofit = builder.client(httpClient.build()).build();

    }

    private static void createInstance2()
    {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        // set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        httpClient.connectTimeout(1, TimeUnit.MINUTES)
                .writeTimeout(1, TimeUnit.MINUTES)
                .readTimeout(1, TimeUnit.MINUTES);

        httpClient.addInterceptor(logging);

        Retrofit.Builder builder =
                new Retrofit.Builder()
                        .baseUrl(SERVER_CENTRAL_IP_OTP)
                        .addConverterFactory(GsonConverterFactory.create());
        retrofit = builder.client(httpClient.build()).build();

    }
}
