
package com.futurity.insurepro.ui.main;


import com.futurity.insurepro.ui.base.MvpPresenter;

public interface MainMvpPresenter<V extends MainView> extends MvpPresenter<V> {
    void getNavigationData();
    void updateUserProfile();
    void checkForLocationPermission();
    void checkForStoragePermission();
    void getLastLocation();
    void getMyCurrentLocation();
}
