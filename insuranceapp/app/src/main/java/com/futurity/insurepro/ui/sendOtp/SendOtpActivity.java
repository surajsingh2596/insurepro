package com.futurity.insurepro.ui.sendOtp;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.futurity.insurepro.R;
import com.futurity.insurepro.components.GlobalConstant;
import com.futurity.insurepro.model.SendOtpModel;
import com.futurity.insurepro.ui.base.BaseActivity;
import com.futurity.insurepro.ui.verifyOtp.VerifyOtpActivity;


public class SendOtpActivity extends BaseActivity implements SendOtpView {

    SendOtpPresenter<SendOtpView> mPresenter;
    private static String TAG="SendOtpactivity";
    SendOtpModel sendOtpModel=new SendOtpModel();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_otp);
        mPresenter=new SendOtpPresenter<>(this);
        mPresenter.onAttach(this);

//        AppSignatureHelper appSignature = new AppSignatureHelper(this);
//        for (String signature : appSignature.getAppSignatures()) {
//            Log.e(TAG, "onCreate: " + signature );
//        }
        setReadTermsHyperlink();
    }

    private void setReadTermsHyperlink(){
        TextView textView=findViewById(R.id.read_text);
        textView.setText(Html.fromHtml(" <a href='https://mycornershop.in/terms-conditions/terms.html'>I accept terms and conditions.</a>"));
        textView.setClickable(true);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    public void onExplore(View view){
        switch(view.getId()){
            case R.id.sendOtp:
                if(validate() && validateTerm()){
                    mPresenter.sendOtp(sendOtpModel);
                }
        }
    }

    private boolean validateTerm() {
        CheckBox checkBox=findViewById(R.id.term);
        if(checkBox.isChecked()){
            return true;
        }else{
            Toast.makeText(this, getString(R.string.agree_terms_accept), Toast.LENGTH_SHORT).show();
            return false;
        }

    }
    private boolean validate(){
        EditText mobileNo=findViewById(R.id.mobile);
        if(mobileNo.getText()!=null){
            if(mobileNo.getText().toString().trim().length()==0){
                Toast.makeText(this, getString(R.string.enter_mobile_no_e), Toast.LENGTH_LONG).show();
                return false;
            }
            if(mobileNo.getText().toString().trim().length()<10){
                Toast.makeText(this, getString(R.string.enter_mobile_no_v), Toast.LENGTH_LONG).show();
                return false;
            }
        }else{
            Toast.makeText(this, getString(R.string.enter_mobile_no_e), Toast.LENGTH_LONG).show();
            return false;
        }
        sendOtpModel.setMobile(mobileNo.getText().toString().trim());
        return true;
    }
    @Override
    public void setTitle() {

    }

    @Override
    public void showLoading() {
        LinearLayout mainContent=findViewById(R.id.progress_bar);
        mainContent.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        LinearLayout mainContent=findViewById(R.id.progress_bar);
        mainContent.setVisibility(View.GONE);
    }

    @Override
    public void isNetworkConnected(boolean val) {
        Toast.makeText(this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onShowMainContent() {
        LinearLayout mainContent=findViewById(R.id.main_content);
        mainContent.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHideMainContent() {
        LinearLayout mainContent=findViewById(R.id.main_content);
        mainContent.setVisibility(View.GONE);
    }

    @Override
    public void getSuccess(SendOtpModel sendOtpModel) {
        Intent intent = new Intent(this, VerifyOtpActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(GlobalConstant.SEND_OTP_MODEL, sendOtpModel);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public void getFailure(String error) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
    }


    @Override
    public void onShowCommonErrorPage() {

    }

    @Override
    public void onHideCommonErrorPage() {

    }

    @Override
    public void logoutUser() {

    }

    @Override
    public void hideKeyboard() {
        GlobalConstant.hideSoftKeyboard(this);
    }

    @Override
    public void forceLogout(String message) {

    }

    @Override
    public void onShowNoData() {

    }

    @Override
    public void onHideNoData() {

    }

    @Override
    public void hideAll() {

    }


}
