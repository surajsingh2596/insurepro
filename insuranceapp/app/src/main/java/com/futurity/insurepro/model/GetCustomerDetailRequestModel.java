package com.futurity.insurepro.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GetCustomerDetailRequestModel implements Serializable {

    @SerializedName("username")
    @Expose
    private String mobile;

    @SerializedName("rid")
    @Expose
    private int rid;

    public GetCustomerDetailRequestModel() {
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public int getRid() {
        return rid;
    }

    public void setRid(int rid) {
        this.rid = rid;
    }
}
