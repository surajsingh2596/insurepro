package com.futurity.insurepro.model;

import java.io.Serializable;

public class SendOtpModel implements Serializable {

    public static final String APIKEY="L6i6XjhWFUyVjT5e2CrIyg";
    public static final String SENDER_Id="APPSMS";
    public static final int CHANNEL=2;
    public static final int DCS=0;
    public static final int ROUTE=13;
    public static final int FLAS_SMS=0;

    private String mobile;

    private String text;

    private String otp;

    public SendOtpModel() {
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }
}
