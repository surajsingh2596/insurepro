package com.futurity.insurepro.ui.verifyOtp;

import android.content.Context;


import com.futurity.insurepro.R;
import com.futurity.insurepro.components.GlobalConstant;
import com.futurity.insurepro.model.CommonResponseModel;
import com.futurity.insurepro.model.LoginModel;
import com.futurity.insurepro.model.LoginRequestModel;
import com.futurity.insurepro.model.LoginResponseModel;
import com.futurity.insurepro.model.SendOtpModel;
import com.futurity.insurepro.model.SendOtpRequestModel;
import com.futurity.insurepro.network.ApiClientInterface;
import com.futurity.insurepro.network.NewApiClient;
import com.futurity.insurepro.ui.base.BasePresenter;
import com.mukesh.OnOtpCompletionListener;
import com.mukesh.OtpView;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class VerifyOtpPresenter<V extends VerifyOtpView> extends BasePresenter<V>
        implements VerifyOtpMvpPresenter<V> {
    private static String TAG="VerifyOtpPresenter";
    private Context mContext;
    private SendOtpModel sendOtpModel;
    private int type;
    public VerifyOtpPresenter(VerifyOtpActivity verifyOtpActivity) {
        mContext=(Context)verifyOtpActivity;
    }

    @Override
    public void onAttach(V baseView) {
        super.onAttach(baseView);
        onStart();
    }
    private void onStart(){

    }
    @Override
    public void resendOtp(SendOtpModel sendOtpModel) {
        sendOtpModel.setOtp(GlobalConstant.getOtp());
        sendOtpModel.setText(GlobalConstant.getOtpMessage(sendOtpModel.getOtp()));
        getMvpView().hideKeyboard();
        getMvpView().showLoading();

        if (!GlobalConstant.isNetworkOnline(mContext)) {
            getMvpView().isNetworkConnected(false);
            getMvpView().hideLoading();
            return;
        }
        SendOtpRequestModel sendOtpRequestModel=new SendOtpRequestModel();
        sendOtpRequestModel.setFlag("Android");
        sendOtpRequestModel.setUsername(sendOtpModel.getMobile());
        sendOtpRequestModel.setMsg(sendOtpModel.getText());
        sendOtpRequestModel.setRid(GlobalConstant.R_ID);
        sendOtpRequestModel.setType(GlobalConstant.TYPE);
        ApiClientInterface apiClientInterface= NewApiClient.getApiClient().create(ApiClientInterface.class);
        apiClientInterface.sendOtp(sendOtpRequestModel).enqueue(new Callback<CommonResponseModel<String>>()
        {
            @Override
            public void onResponse(Call<CommonResponseModel<String>> call, Response<CommonResponseModel<String>> response)
            {
                if(response.isSuccessful()){
                    getMvpView().hideLoading();
                    CommonResponseModel<String> otpResponseModel=response.body();
                    getMvpView().getOtpResendSuccess();

                }else{
                    getMvpView().hideLoading();
                    getMvpView().getOtpResendFailure(mContext.getResources().getString(R.string.server_error));
                }

            }

            @Override
            public void onFailure(Call<CommonResponseModel<String>> call, Throwable t)
            {
                call.cancel();
                if (t instanceof IOException) {
                    getMvpView().getOtpResendFailure(mContext.getResources().getString(R.string.no_internet));
                }
                else {
                    getMvpView().getOtpResendFailure(mContext.getResources().getString(R.string.server_error));
                }
                getMvpView().hideLoading();
            }
        });
    }
    @Override
    public void login(LoginModel loginModel) {

        LoginRequestModel loginRequestModel=new LoginRequestModel();
        loginRequestModel.setUsername(loginModel.getUsername());
        loginRequestModel.setPassword(loginModel.getPassword());
        loginRequestModel.setRid(GlobalConstant.R_ID);
        loginRequestModel.setType(GlobalConstant.TYPE);
        loginRequestModel.setfpage(GlobalConstant.F_PAGE);

        getMvpView().hideKeyboard();
        getMvpView().showLoading();
        if (!GlobalConstant.isNetworkOnline(mContext)) {
            getMvpView().isNetworkConnected(false);
            getMvpView().hideLoading();
            return;
        }
        getMvpView().showLoading();

        ApiClientInterface apiClientInterface= NewApiClient.getApiClient().create(ApiClientInterface.class);
        apiClientInterface.loginUser(loginRequestModel).enqueue(new Callback<CommonResponseModel<LoginResponseModel>>()
        {
            @Override
            public void onResponse(Call<CommonResponseModel<LoginResponseModel>> call, Response<CommonResponseModel<LoginResponseModel>> response)
            {
                getMvpView().hideLoading();
                if(response.isSuccessful()){
                    try{
                        CommonResponseModel<LoginResponseModel> responseModel=response.body();
                        switch(responseModel.getStatus()){
                            case GlobalConstant.RESPONSE_SUCCESS:
                                getMvpView().loginSuccess(responseModel.getData());
                                break;
                            case GlobalConstant.RESPONSE_NO_ACCESS:
                                getMvpView().forceLogout(mContext.getResources().getString(R.string.no_access_allowed));
                                break;
                            case GlobalConstant.RESPONSE_NO_USER:
                                getMvpView().error(mContext.getResources().getString(R.string.wrong_cred));
                                break;
                            default:
                                getMvpView().error(mContext.getResources().getString(R.string.server_error));
                                break;
                        }
                    }catch (Exception e){
                        getMvpView().error(mContext.getResources().getString(R.string.server_error));
                    }

                }else{
                    getMvpView().hideLoading();
                    getMvpView().error(mContext.getResources().getString(R.string.server_error));
                }
            }
            @Override
            public void onFailure(Call<CommonResponseModel<LoginResponseModel>> call, Throwable t)
            {
                call.cancel();
                if (t instanceof IOException) {
                    getMvpView().error(mContext.getResources().getString(R.string.internet_connection_issue));
                }
                else {
                    getMvpView().error(mContext.getResources().getString(R.string.server_error));
                }
                getMvpView().hideLoading();
            }
        });
    }
    @Override
    public void setSendOtpModel(SendOtpModel mSendOtpModel) {
        sendOtpModel=mSendOtpModel;
        updateView();
    }

    @Override
    public void updateView() {
        getMvpView().updateMobileNo("+91 "+sendOtpModel.getMobile());
    }

    @Override
    public void setOtpWatcher(OtpView otpView) {
        otpView.setOtpCompletionListener(new OnOtpCompletionListener() {
            @Override
            public void onOtpCompleted(String pinview) {
                if(pinview.length()==4){
                   verifyOTP(pinview);
                }
            }
        });
    }

    @Override
    public void verifyOTP(String pin) {
        if(pin.equals(sendOtpModel.getOtp())){
            getMvpView().otpMatchSuccess();
        }else{
            getMvpView().otpMatchFailure();
        }
    }

}
