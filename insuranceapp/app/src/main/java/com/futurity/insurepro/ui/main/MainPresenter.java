package com.futurity.insurepro.ui.main;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.util.Log;


import com.futurity.insurepro.R;
import com.futurity.insurepro.components.GlobalConstant;
import com.futurity.insurepro.gps.GetCurrentLocation;
import com.futurity.insurepro.gps.GpsUtils;
import com.futurity.insurepro.gps.MyCurrentLocation;
import com.futurity.insurepro.model.CommonResponseModel;
import com.futurity.insurepro.model.MenuNavigatorRequestModel;
import com.futurity.insurepro.model.NavigationDataModel;
import com.futurity.insurepro.model.UserModel;
import com.futurity.insurepro.model.UserProfileModel;
import com.futurity.insurepro.network.ApiClientInterface;
import com.futurity.insurepro.network.NewApiClient;
import com.futurity.insurepro.ui.base.BasePresenter;
import com.futurity.insurepro.ui.permission.MarshMallowPermission;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainPresenter<V extends MainView> extends BasePresenter<V>
        implements MainMvpPresenter<V> {
    private static String TAG="MainPresenter";
    private Context mContext;
    private UserProfileModel userProfileModel;

    public MainPresenter(MainActivity vehicleClassActivity) {
        mContext=(Context)vehicleClassActivity;
    }


    @Override
    public void onAttach(V baseView) {
        super.onAttach(baseView);
        onStart();
    }
    private void onStart(){
        getMvpView().setTitle();
        if(UserModel.isLogin(mContext)){
            userProfileModel=UserModel.getUser(mContext);
            getMvpView().getUserModel(userProfileModel);
            updateUserProfile();
        }else{
            logoutUser();
        }
    }

    @Override
    public void updateUserProfile() {
        if(userProfileModel.getFirstname()!=null && userProfileModel.getFirstname().length()>0){
            getMvpView().setUserFullName(userProfileModel.getFirstname());
        }
        if(userProfileModel.getProfileImage()!=null && userProfileModel.getProfileImage().length()>0){
            Log.d(TAG, "updateUserProfile2: "+userProfileModel.getProfileImage());
            getMvpView().updateUserProfileImage(userProfileModel.getProfileImage());
        }
    }

    @Override
    public void getNavigationData() {
        getMvpView().hideNavigationAll();
        getMvpView().hideKeyboard();
        getMvpView().showNavigationLoader();
        if (!GlobalConstant.isNetworkOnline(mContext)) {
            getMvpView().hideNavigationAll();
            getMvpView().showNavigationReload();
            return;
        }

        MenuNavigatorRequestModel menuNavigatorRequestModel=new MenuNavigatorRequestModel();
        menuNavigatorRequestModel.setUsername(userProfileModel.getUsername());
        menuNavigatorRequestModel.setPassword(userProfileModel.getPassword());
        menuNavigatorRequestModel.setRid(UserModel.getRid(mContext));
        menuNavigatorRequestModel.setType(GlobalConstant.TYPE);
        ApiClientInterface apiClientInterface= NewApiClient.getApiClient().create(ApiClientInterface.class);
        apiClientInterface.navigationData(menuNavigatorRequestModel).enqueue(new Callback<CommonResponseModel<List<NavigationDataModel>>>()
        {
            @Override
            public void onResponse(Call<CommonResponseModel<List<NavigationDataModel>>> call, Response<CommonResponseModel<List<NavigationDataModel>>> response)
            {
                getMvpView().hideNavigationAll();
                if(response.isSuccessful()){
                    try{
                        CommonResponseModel<List<NavigationDataModel>> responseModel=response.body();
                        switch(responseModel.getStatus()){
                            case GlobalConstant.RESPONSE_SUCCESS:
                                if(responseModel.getData().size()>0){
                                    getMvpView().showNavigationItem();
                                    getMvpView().navigationDataSuccess(responseModel.getData());
                                }
                                break;
                            case GlobalConstant.RESPONSE_NO_ACCESS:
                                getMvpView().forceLogout(mContext.getResources().getString(R.string.no_access_allowed));
                                break;
                            case GlobalConstant.RESPONSE_NO_USER:
                                getMvpView().forceLogout(mContext.getResources().getString(R.string.no_user_found));
                                break;
                            default:
                                getMvpView().showNavigationReload();
                                break;
                        }
                    }catch (Exception e){
                        getMvpView().showNavigationReload();
                    }

                }else{
                    getMvpView().hideNavigationAll();
                    getMvpView().showNavigationReload();
                }
            }
            @Override
            public void onFailure(Call<CommonResponseModel<List<NavigationDataModel>>> call, Throwable t)
            {
                call.cancel();
                getMvpView().showNavigationReload();
                getMvpView().hideNavigationAll();
            }
        });
    }
    @Override
    public void checkForLocationPermission() {
        MarshMallowPermission marshMallowPermission=new MarshMallowPermission((Activity)mContext);
        if(!marshMallowPermission.checkPermissionForLocation()){
            getMvpView().ifLocationPermissionGranted(false);
        }else{
            getMvpView().ifLocationPermissionGranted(true);
        }
    }

    @Override
    public void getLastLocation() {
        // check punching type
        GpsUtils gpsUtils=new GpsUtils(mContext);
        gpsUtils.turnGPSOn(isGPSEnable->{
            if(isGPSEnable){
               getMyCurrentLocation();
            }

        });
    }
    @Override
    public void getMyCurrentLocation() {
        MyCurrentLocation myCurrentLocation=new MyCurrentLocation(mContext);
        myCurrentLocation.getCurrentLocation(new GetCurrentLocation() {
            @Override
            public void onSuccess(Location location) {
                getMvpView().getCurrentLocation(location);
            }

            @Override
            public void locationNotFound() {

            }

            @Override
            public void onError() {

            }
        });
    }

    @Override
    public void checkForStoragePermission() {
        MarshMallowPermission marshMallowPermission=new MarshMallowPermission((Activity)mContext);
        if(!marshMallowPermission.checkPermissionForExternalStorage()){
            getMvpView().ifStoragePermissionGranted(false);
        }else{
            getMvpView().ifStoragePermissionGranted(true);
        }
    }
}
