package com.futurity.insurepro.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UpdateDeviceTokenRequestModel extends CommonLoginRequestModel implements Serializable {

    @SerializedName("deviceid")
    @Expose
    private String deviceId;

    @SerializedName("cid")
    @Expose
    private int cid;

    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("rid")
    @Expose
    private int rid;

    public UpdateDeviceTokenRequestModel() {
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getRid() {
        return rid;
    }

    public void setRid(int rid) {
        this.rid = rid;
    }
}
