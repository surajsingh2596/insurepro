package com.futurity.insurepro.ui.permission;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.futurity.insurepro.model.PermissionModel;
import com.futurity.insurepro.model.SendOtpModel;
import com.futurity.insurepro.model.UserModel;
import com.futurity.insurepro.ui.base.BasePresenter;

import java.util.ArrayList;
import java.util.List;


public class PermissionPresenter<V extends PermissionView> extends BasePresenter<V>
        implements PermissionMvpPresenter<V> {
    private static String TAG="VerifyOtpPresenter";
    private Context mContext;
    private SendOtpModel sendOtpModel;
    private int type;
    private  PermissionActivity permissionActivity;
    String[] appPermissions={Manifest.permission.READ_CALL_LOG,Manifest.permission.READ_PHONE_STATE};
    List<PermissionModel> permissionModels=new ArrayList<>();
    public PermissionPresenter(PermissionActivity mPermissionActivity) {
        mContext=(Context)mPermissionActivity;
        permissionActivity=mPermissionActivity;
    }

    @Override
    public void onAttach(V baseView) {
        super.onAttach(baseView);
        onStart();
    }
    private void onStart(){
        //checkPermissions();
    }

    @Override
    public boolean getAllPermission() {
        boolean allPermit=true;
        int result;
        for (String p:appPermissions) {
            result = ContextCompat.checkSelfPermission(mContext,p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                allPermit=false;
                break;
            }

        }
        if(allPermit){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (Settings.canDrawOverlays(mContext)) {
                    allPermit=true;
                }else{
                    allPermit=false;
                }
            }else{
                allPermit=true;
            }
        }
       return allPermit;
    }

    @Override
    public void checkAllPermit() {
        getMvpView().getAllPermit(getAllPermission());
    }

    @Override
    public void checkPermissions() {
        permissionModels.clear();
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p:appPermissions) {
            PermissionModel permissionModel=new PermissionModel();
            permissionModel.setPermission(p);
            permissionModel.setType(PermissionModel.TYPE_ALLOWED);

            result = ContextCompat.checkSelfPermission(mContext,p);
            Log.d(TAG, "checkPermissions: "+result);
            if (result != PackageManager.PERMISSION_GRANTED) {
                permissionModel.setType(PermissionModel.TYPE_DENIED);
                if(!ActivityCompat.shouldShowRequestPermissionRationale(permissionActivity,p)){
                    if(p.equals(Manifest.permission.READ_CALL_LOG)){
                        if(UserModel.isFirstTimeAskingPermission2(mContext)){
                            UserModel.firstTimeAskingPermission2(mContext,false);
                        }else{
                            permissionModel.setType(PermissionModel.TYPE_SET_NEVER_ASK_AGAIN);
                        }
                    }
                    if(p.equals(Manifest.permission.READ_PHONE_STATE)){
                        if(UserModel.isFirstTimeAskingPermission1(mContext)){
                            UserModel.firstTimeAskingPermission1(mContext,false);
                        }else{
                            permissionModel.setType(PermissionModel.TYPE_SET_NEVER_ASK_AGAIN);
                        }
                    }


                }
            }
            permissionModels.add(permissionModel);
        }
        getMvpView().getPermissionList(permissionModels);
        updateButtons();
    }

    @Override
    public void updateButtons() {
        getMvpView().hideAllButtons();
        boolean showEnabled=true;
        for (PermissionModel permissionModel:permissionModels){
           // Log.d(TAG, "updateButtons: "+permissionModel.getType()+"   "+permissionModel.getPermission());
            if(permissionModel.getType()==PermissionModel.TYPE_SET_NEVER_ASK_AGAIN){
                showEnabled=false;
                getMvpView().showButtonDisabledSetting();
                break;
            }
            else if(permissionModel.getType()==PermissionModel.TYPE_DENIED){
                showEnabled=false;
                getMvpView().showButtonDisabled();
                break;
            }
        }
        if(showEnabled)
            getMvpView().showButtonEnabled();
    }

    @Override
    public void setPermissionModelList(List<PermissionModel> permissionModelList) {
        permissionModels=permissionModelList;
        updateButtons();
    }

    @Override
    public void checkOverlay() {
        getMvpView().hideAllOverlayButtons();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (Settings.canDrawOverlays(mContext)) {
               getMvpView().showOverLayEnabled();
            }else{
                getMvpView().showOverlayDisabled();
            }
        }else{
            getMvpView().showOverLayEnabled();
        }

    }

    @Override
    public void drawOverlay(boolean val) {
        checkOverlay();
    }


}
