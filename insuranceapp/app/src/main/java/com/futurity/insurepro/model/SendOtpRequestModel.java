package com.futurity.insurepro.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SendOtpRequestModel extends CommonLoginRequestModel implements Serializable {

    @SerializedName("msg")
    @Expose
    private String msg;

    @SerializedName("flag")
    @Expose
    private String flag;

    @SerializedName("rid")
    @Expose
    private int rid;

    @SerializedName("type")
    @Expose
    private String type;

    public SendOtpRequestModel() {
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public int getRid() {
        return rid;
    }

    public void setRid(int rid) {
        this.rid = rid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
