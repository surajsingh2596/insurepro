package com.futurity.insurepro.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class NavigationDataModel implements Serializable {
    private boolean logout;

    @SerializedName("Title")
    @Expose
    private String title;

    @SerializedName("URL")
    @Expose
    private String url;

    public NavigationDataModel() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isLogout() {
        return logout;
    }

    public void setLogout(boolean logout) {
        this.logout = logout;
    }
}
