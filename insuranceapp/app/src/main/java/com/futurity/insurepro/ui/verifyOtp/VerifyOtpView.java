package com.futurity.insurepro.ui.verifyOtp;


import com.futurity.insurepro.model.LoginResponseModel;
import com.futurity.insurepro.ui.base.BaseView;

public interface VerifyOtpView extends BaseView {
    void updateMobileNo(String mobileNo);
    void getOtpResendSuccess();
    void getOtpResendFailure(String error);
    void otpMatchSuccess();
    void otpMatchFailure();

    void error(String message);
    void loginSuccess(LoginResponseModel loginResponseModel);
}
