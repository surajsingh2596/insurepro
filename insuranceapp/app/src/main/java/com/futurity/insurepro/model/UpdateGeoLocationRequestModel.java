package com.futurity.insurepro.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UpdateGeoLocationRequestModel extends CommonLoginRequestModel implements Serializable {

    @SerializedName("lattitude")
    @Expose
    private double lattitude;

    @SerializedName("longitude")
    @Expose
    private double longitude;

    @SerializedName("user_id")
    @Expose
    private int userId;

    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("rid")
    @Expose
    private int rid;

    public UpdateGeoLocationRequestModel() {
    }

    public double getLattitude() {
        return lattitude;
    }

    public void setLattitude(double lattitude) {
        this.lattitude = lattitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getRid() {
        return rid;
    }

    public void setRid(int rid) {
        this.rid = rid;
    }
}
