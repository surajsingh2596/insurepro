package com.futurity.insurepro.ui.splash;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

import com.futurity.insurepro.R;
import com.futurity.insurepro.components.GlobalConstant;
import com.futurity.insurepro.model.CommonResponseModel;
import com.futurity.insurepro.model.LoginRequestModel;
import com.futurity.insurepro.model.LoginResponseModel;
import com.futurity.insurepro.model.UserModel;
import com.futurity.insurepro.network.ApiClientInterface;
import com.futurity.insurepro.network.NewApiClient;
import com.futurity.insurepro.ui.base.BasePresenter;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashPresenter<V extends SplashView> extends BasePresenter<V>
        implements SplashMvpPresenter<V> {
    private static String TAG="MainPresenter";
    private Context mContext;
    public SplashPresenter(SplashActivity vehicleClassActivity) {
        mContext=(Context)vehicleClassActivity;
    }


    @Override
    public void onAttach(V baseView) {
        super.onAttach(baseView);
        onStart();
    }
    private void onStart(){
        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    checkIfUserLogin();
                }
                catch (Exception e)
                {
                    Log.d("qweqweqwe", "run: ");
                    e.printStackTrace();
                }
            }
        }, 3000);
    }

    @Override
    public void checkIfUserLogin() {
        if(UserModel.isLogin(mContext)){
            login();
        }else{
            loginMobileActivity();
        }
    }
    private void loginMobileActivity(){
        Class<?> aClass=null;
        if(GlobalConstant.LOGIN_TYPE==1){
            getMvpView().loginActivity();
        }else if(GlobalConstant.LOGIN_TYPE==2){
            getMvpView().sendOtpActivity();
        }
    }

    @Override
    public void login() {

        LoginRequestModel loginRequestModel=new LoginRequestModel();
        loginRequestModel.setUsername(UserModel.getUser(mContext).getUsername());
        loginRequestModel.setPassword(UserModel.getUser(mContext).getPassword());
        loginRequestModel.setRid(UserModel.getRid(mContext));
        loginRequestModel.setType(GlobalConstant.TYPE);
        loginRequestModel.setfpage(GlobalConstant.F_PAGE);

        getMvpView().hideKeyboard();
        getMvpView().showProgressD();
        if (!GlobalConstant.isNetworkOnline(mContext)) {
            getMvpView().isNetworkConnected(false);
            getMvpView().hideProgressD();
            return;
        }

        ApiClientInterface apiClientInterface= NewApiClient.getApiClient().create(ApiClientInterface.class);
        apiClientInterface.loginUser(loginRequestModel).enqueue(new Callback<CommonResponseModel<LoginResponseModel>>()
        {
            @Override
            public void onResponse(Call<CommonResponseModel<LoginResponseModel>> call, Response<CommonResponseModel<LoginResponseModel>> response)
            {
                getMvpView().hideProgressD();
                if(response.isSuccessful()){
                    try{
                        CommonResponseModel<LoginResponseModel> responseModel=response.body();
                        switch(responseModel.getStatus()){
                            case GlobalConstant.RESPONSE_SUCCESS:
                                getMvpView().loginSuccess(responseModel.getData());
                                break;
                            case GlobalConstant.RESPONSE_NO_ACCESS:
                                getMvpView().forceLogout(mContext.getResources().getString(R.string.no_access_allowed));
                                break;
                            case GlobalConstant.RESPONSE_NO_USER:
                                getMvpView().error(mContext.getResources().getString(R.string.wrong_cred));
                                break;
                            default:
                                getMvpView().error(mContext.getResources().getString(R.string.server_error));
                                break;
                        }
                    }catch (Exception e){
                        getMvpView().error(mContext.getResources().getString(R.string.server_error));
                    }

                }else{
                    getMvpView().hideProgressD();
                    getMvpView().error(mContext.getResources().getString(R.string.server_error));
                }
            }
            @Override
            public void onFailure(Call<CommonResponseModel<LoginResponseModel>> call, Throwable t)
            {
                call.cancel();
                if (t instanceof IOException) {
                    getMvpView().error(mContext.getResources().getString(R.string.internet_connection_issue));
                }
                else {
                    getMvpView().error(mContext.getResources().getString(R.string.server_error));
                }
                getMvpView().hideProgressD();
            }
        });
    }
}
