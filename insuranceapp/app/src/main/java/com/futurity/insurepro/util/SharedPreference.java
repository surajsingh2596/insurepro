package com.futurity.insurepro.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.location.Location;
import android.preference.PreferenceManager;

import com.futurity.insurepro.components.GlobalConstant;


/**
 * Created by Deepak on 23-07-2015.
 */
public class SharedPreference {

    SharedPreferences sp;
    static final String KEY_REQUESTING_LOCATION_UPDATES = "requesting_locaction_updates";
    public SharedPreference(Context con) {
        sp = con.getSharedPreferences(GlobalConstant.PREF_NAME, Context.MODE_PRIVATE);
    }


    public void setValueString(String paramName, String paramValue) {
        Editor et = sp.edit();
        et.putString(paramName, paramValue);
        et.commit();
    }

    public String getValueString(String paramName) {
        return sp.getString(paramName, "");
    }

    public void setValueInt(String paramName, int paramValue) {
        Editor et = sp.edit();
        et.putInt(paramName, paramValue);
        et.commit();
    }

    public int getValueInt(String paramName) {
        return sp.getInt(paramName, 0);
    }

    public void setValueBool(String paramName, boolean paramValue) {
        Editor et = sp.edit();
        et.putBoolean(paramName, paramValue);
        et.commit();
    }

    public boolean getValueBoolean(String paramName) {
        return sp.getBoolean(paramName, false);
    }

    public void setValueDouble(String paramName, Double paramValue) {
        Editor et = sp.edit();
        et.putLong(paramName, Double.doubleToRawLongBits(paramValue));
        et.commit();

    }

    public Double getValueDouble(String paramName) {

        return Double.longBitsToDouble(sp.getLong(paramName, Double.doubleToLongBits(0)));
    }

    public void setValueLong(String paramName, long paramValue) {
        Editor et = sp.edit();
        et.putLong(paramName, paramValue);
        et.commit();

    }

    public long getValueLong(String paramName) {

        return (sp.getLong(paramName, Double.doubleToLongBits(0)));
    }


    public void clearAll() {
        sp.edit().clear().commit();
    }

    public boolean getPermissionBoolean(String paramName) {
        return sp.getBoolean(paramName, true);
    }
    public boolean getBooleanVal(String paramName) {
        return sp.getBoolean(paramName, false);
    }
    /**
     * Returns true if requesting location updates, otherwise returns false.
     *
     * @param context The {@link Context}.
     */
    public static boolean requestingLocationUpdates(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(KEY_REQUESTING_LOCATION_UPDATES, false);
    }

    /**
     * Stores the location updates state in SharedPreferences.
     * @param requestingLocationUpdates The location updates state.
     */
    public static void setRequestingLocationUpdates(Context context, boolean requestingLocationUpdates) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putBoolean(KEY_REQUESTING_LOCATION_UPDATES, requestingLocationUpdates)
                .apply();
    }

    /**
     * Returns the {@code location} object as a human readable string.
     * @param location  The {@link Location}.
     */
    public static String getLocationText(Location location) {
        return location == null ? "Unknown location" :
                "(" + location.getLatitude() + ", " + location.getLongitude() + ")";
    }
}
