package com.futurity.insurepro.ui.login;

import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.futurity.insurepro.R;
import com.futurity.insurepro.appSignature.AppSignatureHelper;
import com.futurity.insurepro.components.GlobalConstant;
import com.futurity.insurepro.model.LoginModel;
import com.futurity.insurepro.model.LoginResponseModel;
import com.futurity.insurepro.model.UserModel;
import com.futurity.insurepro.model.UserProfileModel;
import com.futurity.insurepro.ui.base.BaseActivity;
import com.futurity.insurepro.ui.main.MainActivity;
import com.futurity.insurepro.ui.sendOtp.SendOtpActivity;


public class LoginActivity extends BaseActivity implements LoginView {

    LoginPresenter<LoginView> mPresenter;

    private static String TAG="ParkingDetailActivity";
    private TextView titleBar;
    private EditText username,password;
    private LoginModel loginModel=new LoginModel();
    private static final int APP_DRAW_CODE=23465;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        updateDeviceToken=false;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            Window w = getWindow();
//            w.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
//            w.setStatusBarColor(getColor(R.color.theme_color_dark));
//        }
        username=findViewById(R.id.username);
        password=findViewById(R.id.password);

        mPresenter=new LoginPresenter<>(this);
        mPresenter.onAttach(this);
        //createIso();
        //overdraw();
        setReadTermsHyperlink();
        hashget();
    }

    private void hashget(){
        String v=AppSignatureHelper.hash("com.futurity.com.futurity.insurepro","3082058830820370a003020102021437cf8cfb153e06096e58b1aa5ead1da27c6af4d2300d06092a864886f70d01010b05003074310b3009060355040613025553311330110603550408130a43616c69666f726e6961311630140603550407130d4d6f756e7461696e205669657731143012060355040a130b476f6f676c6520496e632e3110300e060355040b1307416e64726f69643110300e06035504031307416e64726f69643020170d3230313232393039353034325a180f32303530313232393039353034325a3074310b3009060355040613025553311330110603550408130a43616c69666f726e6961311630140603550407130d4d6f756e7461696e205669657731143012060355040a130b476f6f676c6520496e632e3110300e060355040b1307416e64726f69643110300e06035504031307416e64726f696430820222300d06092a864886f70d01010105000382020f003082020a0282020100bd7e78096b28b4758866f5fbed87fa36c7ea01c58c91a94ba80a916d86ccebb744bda405cdfef4df955aa5fa9eba3ff459badfc6e300e7179321dcf98c1c5d3bf707b5357b3fb94e7d800b120acd1a2a33de6ee0f526ad06c30e997e9b9b0e9605a8848cdb6fcad8bd96ac445d1c81df55f6663c217591367b5ab8eeba245fdf87d38d248a86b85177da3ddb09fc18d0ff5943cec134f03681114b411c00e4f1923681e7ab9eb8aca55415486fa9a864cc80d791b8fffd4d7769b49e952e5f427202e0b51df6a7f3e23b310809c323a7302cff71153516919c9d65d7fc87734608110c75d51fd3abb5ff63e86f672c17a86977dcea7775acb3cfe2ddf89d729dce47498a8366be5fc90cdb2ca91e84766dacc248aa217de5dcc513acc39e93b6e93f209bd67539310715434f2f58d1d6033896614fd9bcb4de7bce38dcf96db73fa64db02ef6f3000d3d19a1b71b2b2465412eef09fb565da483b92df131675b8753eda948a8c0ef8563f4e3b91e5013d72ff8eaa2bf3a90ea3b67c222033ead40167a32447521708ac72fc8d8e774f1eb98bfb15e147f29ecac2f2c2c02e8c10bac0b90cee1a476e39a5328203a8550e3b683c7b83e092e133395d559f2465fb099f5e30eed28d678095614d5a2751d03f2b864f6b92f4ec34b4b0ffbc4c8a7ca328cd83b53f21c948bed6842a328ede9417d326bf188fdb3c341d0ab9e04730203010001a310300e300c0603551d13040530030101ff300d06092a864886f70d01010b050003820201003f796984b8abdec6f190aad9e67b3858e8383aafbac0bb6a4107aa4d3c227b87a5b0ac0c6bb8d1f80183925d41f5c4cabf237f258255a82d39fd6de0ff1c69f283c3c15c1cf101051a5e35014fa56d80848c94cd08675cd53e36bba64e77105eef02dd7c80eca1cea90077fab4dc420cbbc8ba3fa8e64bc78185707a3d327b16aba740f327c1024e96f4cacd4880c8101d94b757283fabc9d1c56e31f9d72916dae51041b3259354331423e5f184ed953a0978b0916958ed834e6a95a7d4bf0f7e14b8c336ab35078b57fe9cb6f353706b9b4f0307a4189f3393aa3decdf5fa181c83e111f1b244388015bd286c3ddc2c5b45f609f56f4749a13c7356af6eb4b8419d8833ed5b7d05251165ec4312a10fbf917feafa54df4cc320c13109163da67bebb01684f576824b090edecd655c87a29fce256568848b9d584eaf45ce95cce0d7e42087fc5ecd163354b155678ff3cd019b1a2d72bb466bae06bd0fa6d8be23661d5092044bd892070adf445bc37940520efb31307c173c273a2ccb7345487fbbafd6df11fe25fb99174f4eeba09132c95e6287f224dfd9e02b5220c4ca4c5c8ab4cb4747625995328a102131bad549674da1fc57855bbaf4a7b77d7526a59dc9bb8efddb7ebf048c8db4546350dd78bc1f65c5882ff5d6b6cb1213221b3831ef75779395c5ddd19accdaca04f96bd2ba5552db99d5e45c2413bbeac2504");

        Log.d(TAG, "hashget: "+v);
    }
    private void setReadTermsHyperlink(){
        TextView textView=findViewById(R.id.read_text);
        textView.setText(Html.fromHtml(" <a href='https://mycornershop.in/terms-conditions/terms.html'>I accept terms and conditions.</a>"));
        textView.setClickable(true);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }
    private void overdraw(){
        WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.TYPE_ACCESSIBILITY_OVERLAY |
                WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY,
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL |
                        WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSPARENT);

        params.height = WindowManager.LayoutParams.MATCH_PARENT;
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.format = PixelFormat.TRANSLUCENT;

        params.gravity = Gravity.TOP;

        LinearLayout ly = new LinearLayout(this);
        ly.setBackgroundColor(getResources().getColor(R.color.transparent));
        ly.setOrientation(LinearLayout.VERTICAL);
        View hiddenInfo = getLayoutInflater().inflate(R.layout.popup_after_call, ly, false);
        Button button=hiddenInfo.findViewById(R.id.btSignIn);
        button.setOnClickListener(v->{
            wm.removeView(ly);
        });
        ly.addView(hiddenInfo);
        wm.addView(ly, params);
    }
    public void loginUser(View view){
        if(validate() && validateTerm()){
            mPresenter.login(loginModel);
        }
    }
    public void mobileLogin(View view){
        Intent intent = new Intent(this, SendOtpActivity.class);
        startActivity(intent);

    }
    @Override
    public void loginSuccess(LoginResponseModel loginResponseModel) {

        UserProfileModel userProfileModel=new UserProfileModel();
        userProfileModel.setId(loginResponseModel.getUserId());
        userProfileModel.setRedirectUrl(loginResponseModel.getUrl());
        userProfileModel.setFirstname(loginResponseModel.getName());
        userProfileModel.setProfileImage(loginResponseModel.getProfilePic());
        userProfileModel.setUsername(loginResponseModel.getUsername());
        userProfileModel.setPassword(GlobalConstant.DEFAULT_PASSWORD);

        UserModel.setRid(this,loginResponseModel.getRid());
        UserModel.login(this,userProfileModel);
        Log.d(TAG, "loginSuccess: ");
        Intent i = new Intent(LoginActivity.this, MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(i);
    }
    @Override
    public boolean validate() {
        if(username.getText()!=null && password.getText()!=null){
            if(username.getText().toString().trim().length()>0 && password.getText().toString().trim().length()>0){
                loginModel.setUsername(username.getText().toString().trim());
                loginModel.setPassword(password.getText().toString().trim());
                return true;
            }
        }
        Toast.makeText(this, getString(R.string.fill_all_the_fields), Toast.LENGTH_SHORT).show();
        return false;
    }

    @Override
    public boolean validateTerm() {
        CheckBox checkBox=findViewById(R.id.term);
        if(checkBox.isChecked()){
            return true;
        }else{
            Toast.makeText(this, getString(R.string.agree_terms_accept), Toast.LENGTH_SHORT).show();
            return false;
        }

    }

    @Override
    public void error(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setTitle() {

    }

    @Override
    public void showLoading() {
        LinearLayout progressBar=findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        LinearLayout progressBar=findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void isNetworkConnected(boolean val) {
        if(!val){
            Toast.makeText(this, getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onShowMainContent() {
//        LinearLayout progressBar=findViewById(R.id.main_content);
//        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHideMainContent() {
//        LinearLayout progressBar=findViewById(R.id.main_content);
//        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onShowCommonErrorPage() {

    }

    @Override
    public void onHideCommonErrorPage() {

    }

    @Override
    public void onShowNoData() {

    }

    @Override
    public void onHideNoData() {

    }

    @Override
    public void logoutUser() {

    }

    @Override
    public void hideKeyboard() {
        GlobalConstant.hideSoftKeyboard(this);
    }

    @Override
    public void forceLogout(String message) {
      super.forceLogout(message);
    }
    @Override
    public void hideAll() {

    }
}
