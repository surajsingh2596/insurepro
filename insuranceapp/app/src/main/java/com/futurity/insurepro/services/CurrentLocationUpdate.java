package com.futurity.insurepro.services;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;


import com.futurity.insurepro.components.GlobalConstant;
import com.futurity.insurepro.model.CommonResponseModel;
import com.futurity.insurepro.model.CurrentLocationModel;
import com.futurity.insurepro.model.UpdateGeoLocationRequestModel;
import com.futurity.insurepro.model.UserModel;
import com.futurity.insurepro.model.UserProfileModel;
import com.futurity.insurepro.network.ApiClientInterface;
import com.futurity.insurepro.network.NewApiClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CurrentLocationUpdate extends JobIntentService
{
    public static final String TAG = CurrentLocationUpdate.class.getSimpleName();

    public static void enqueueWork(Context context, Intent work) {
        enqueueWork(context, CurrentLocationUpdate.class, 123, work);
    }
    public CurrentLocationUpdate()
    {
        super();
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        try{
            if(!UserModel.isLogin(getApplicationContext()))
                return;

            UserProfileModel userProfileModel=UserModel.getUser(getApplicationContext());
            Bundle bundle = intent.getExtras();
            CurrentLocationModel orderHistoryModel = (CurrentLocationModel) bundle.getSerializable(GlobalConstant.CURRENT_LOCATION);

            if (!GlobalConstant.isNetworkOnline(getApplication())) {
                return;
            }
            UpdateGeoLocationRequestModel updateGeoLocationRequestModel=new UpdateGeoLocationRequestModel();
            updateGeoLocationRequestModel.setLattitude(orderHistoryModel.getLatitude());
            updateGeoLocationRequestModel.setLongitude(orderHistoryModel.getLongitude());
            updateGeoLocationRequestModel.setUserId(userProfileModel.getId());
            updateGeoLocationRequestModel.setUsername(userProfileModel.getUsername());
            updateGeoLocationRequestModel.setPassword(userProfileModel.getPassword());
            updateGeoLocationRequestModel.setRid(UserModel.getRid(getApplicationContext()));
            updateGeoLocationRequestModel.setType(GlobalConstant.TYPE);
            ApiClientInterface apiClientInterface= NewApiClient.getApiClient().create(ApiClientInterface.class);
            apiClientInterface.updateGeoLocation(updateGeoLocationRequestModel).enqueue(new Callback<CommonResponseModel<String>>()
            {
                @Override
                public void onResponse(Call<CommonResponseModel<String>> call, Response<CommonResponseModel<String>> response)
                {
                    if(response.isSuccessful()){
                    }else{
                    }

                }

                @Override
                public void onFailure(Call<CommonResponseModel<String>> call, Throwable t)
                {
                    call.cancel();
                }
            });

        }catch (Exception e){

        }
    }
}
