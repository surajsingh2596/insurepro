package com.futurity.insurepro.services;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;


import com.futurity.insurepro.components.GlobalConstant;
import com.futurity.insurepro.model.CommonResponseModel;
import com.futurity.insurepro.model.SendDeviceTokenModel;
import com.futurity.insurepro.model.UpdateDeviceTokenRequestModel;
import com.futurity.insurepro.model.UserModel;
import com.futurity.insurepro.model.UserProfileModel;
import com.futurity.insurepro.network.ApiClientInterface;
import com.futurity.insurepro.network.NewApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SendTokenToServerService extends IntentService
{
    public static final String TAG = SendTokenToServerService.class.getSimpleName();

    public SendTokenToServerService()
    {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent)
    {
        try{
            FirebaseInstanceId.getInstance().getInstanceId()
                    .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                        @Override
                        public void onComplete(@NonNull Task<InstanceIdResult> task) {
                            if (!task.isSuccessful()) {
                                Log.w(TAG, "getInstanceId failed", task.getException());
                                return;
                            }
                            // Get new Instance ID token
                            String token = task.getResult().getToken();
                            Log.d(TAG, "onComplete: "+token);
                            String oldToken= UserModel.getDeviceTokeSendSuccess(SendTokenToServerService.this);
                            if(!oldToken.equals("") && oldToken.equals(token))
                                return;

                            if(UserModel.isLogin(SendTokenToServerService.this)){
                                String lastToken= UserModel.getDeviceTokeSendSuccess(SendTokenToServerService.this);
                                if(!lastToken.equals("") && lastToken.equals(token))
                                    return;

                                UserProfileModel userModel=UserModel.getUser(SendTokenToServerService.this);
                                SendDeviceTokenModel sendDeviceTokenModel=new SendDeviceTokenModel();
                                sendDeviceTokenModel.setDeviceToken(token);

                                UpdateDeviceTokenRequestModel updateDeviceTokenRequestModel=new UpdateDeviceTokenRequestModel();
                                updateDeviceTokenRequestModel.setCid(userModel.getId());
                                updateDeviceTokenRequestModel.setDeviceId(token);
                                updateDeviceTokenRequestModel.setUsername(userModel.getUsername());
                                updateDeviceTokenRequestModel.setPassword(userModel.getPassword());
                                updateDeviceTokenRequestModel.setRid(UserModel.getRid(getApplicationContext()));
                                updateDeviceTokenRequestModel.setType(GlobalConstant.TYPE);

                                ApiClientInterface apiClientInterface= NewApiClient.getApiClient().create(ApiClientInterface.class);
                                apiClientInterface.updateDeviceToken(updateDeviceTokenRequestModel).enqueue(new Callback<CommonResponseModel<String>>()
                                {
                                    @Override
                                    public void onResponse(Call<CommonResponseModel<String>> call, Response<CommonResponseModel<String>> response)
                                    {
                                        if(response.isSuccessful()){
                                            UserModel.setDeviceTokeSendSuccess(SendTokenToServerService.this,token);
                                        }else{
                                            UserModel.setDeviceTokeSendSuccess(SendTokenToServerService.this,"");
                                        }
                                    }
                                    @Override
                                    public void onFailure(Call<CommonResponseModel<String>> call, Throwable t)
                                    {
                                        call.cancel();
                                        UserModel.setDeviceTokeSendSuccess(SendTokenToServerService.this,"");
                                    }
                                });
                            }
                        }
                    });

        }catch(Exception e){
            UserModel.setDeviceTokeSendSuccess(SendTokenToServerService.this,"");
        }

    }


}
