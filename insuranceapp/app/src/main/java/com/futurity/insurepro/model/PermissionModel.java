package com.futurity.insurepro.model;

import java.io.Serializable;

public class PermissionModel implements Serializable {
    public static final int TYPE_ALLOWED=1;
    public static final int TYPE_SET_NEVER_ASK_AGAIN=2;
    public static final int TYPE_DENIED=3;
    private String permission;
    private int type;
    public PermissionModel() {
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
