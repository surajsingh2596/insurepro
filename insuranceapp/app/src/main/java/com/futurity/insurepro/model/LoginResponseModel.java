package com.futurity.insurepro.model;

import com.futurity.insurepro.components.GlobalConstant;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LoginResponseModel implements Serializable {
    @SerializedName("user_id")
    @Expose
    private int userId;

    @SerializedName("url")
    @Expose
    private String url;

    @SerializedName("Customer_Name")
    @Expose
    private String name;

    @SerializedName("profile_image")
    @Expose
    private String profilePic;

    @SerializedName("username")
    @Expose
    private String username;

    private String password;

    @SerializedName("rid")
    @Expose
    private int rid;

    public LoginResponseModel() {
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return GlobalConstant.DEFAULT_PASSWORD;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getRid() {
        return rid;
    }

    public void setRid(int rid) {
        this.rid = rid;
    }
}
