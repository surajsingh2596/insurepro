package com.futurity.insurepro.gps;

import android.location.Location;

public interface GetCurrentLocation {
    void onSuccess(Location location);
    void locationNotFound();
    void onError();
}
