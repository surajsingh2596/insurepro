package com.futurity.insurepro.ui.login;

import android.content.Context;

import com.futurity.insurepro.R;
import com.futurity.insurepro.components.GlobalConstant;
import com.futurity.insurepro.model.CommonResponseModel;
import com.futurity.insurepro.model.LoginModel;
import com.futurity.insurepro.model.LoginRequestModel;
import com.futurity.insurepro.model.LoginResponseModel;
import com.futurity.insurepro.model.UserModel;
import com.futurity.insurepro.network.ApiClientInterface;
import com.futurity.insurepro.network.NewApiClient;
import com.futurity.insurepro.ui.base.BasePresenter;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginPresenter<V extends LoginView> extends BasePresenter<V>
        implements LoginMvpPresenter<V> {
    private Context context;
    private static String TAG="asd";
    public int maxDistance=5;
    private Context mContext;
    public LoginPresenter(LoginActivity loginActivity) {
        mContext=(Context)loginActivity;
    }

    @Override
    public void onAttach(V baseView) {
        super.onAttach(baseView);
        setRidVal();

    }

    private void setRidVal(){
        UserModel.setRid(mContext,GlobalConstant.R_ID);
    }
    @Override
    public void login(LoginModel loginModel) {

        LoginRequestModel loginRequestModel=new LoginRequestModel();
        loginRequestModel.setUsername(loginModel.getUsername());
        loginRequestModel.setPassword(loginModel.getPassword());
        loginRequestModel.setRid(UserModel.getRid(mContext));
        loginRequestModel.setType(GlobalConstant.TYPE);
        loginRequestModel.setfpage(GlobalConstant.F_PAGE);

        getMvpView().hideKeyboard();
        getMvpView().showLoading();
        if (!GlobalConstant.isNetworkOnline(mContext)) {
            getMvpView().isNetworkConnected(false);
            getMvpView().hideLoading();
            return;
        }
        getMvpView().showLoading();

        ApiClientInterface apiClientInterface= NewApiClient.getApiClient().create(ApiClientInterface.class);
        apiClientInterface.loginUser(loginRequestModel).enqueue(new Callback<CommonResponseModel<LoginResponseModel>>()
        {
            @Override
            public void onResponse(Call<CommonResponseModel<LoginResponseModel>> call, Response<CommonResponseModel<LoginResponseModel>> response)
            {
                getMvpView().hideLoading();
                if(response.isSuccessful()){
                    try{
                        CommonResponseModel<LoginResponseModel> responseModel=response.body();
                        switch(responseModel.getStatus()){
                            case GlobalConstant.RESPONSE_SUCCESS:
                                getMvpView().loginSuccess(responseModel.getData());
                                break;
                            case GlobalConstant.RESPONSE_NO_ACCESS:
                                getMvpView().forceLogout(mContext.getResources().getString(R.string.no_access_allowed));
                                break;
                            case GlobalConstant.RESPONSE_NO_USER:
                                getMvpView().error(mContext.getResources().getString(R.string.wrong_cred));
                                break;
                            default:
                                getMvpView().error(mContext.getResources().getString(R.string.server_error));
                                break;
                        }
                    }catch (Exception e){
                        getMvpView().error(mContext.getResources().getString(R.string.server_error));
                    }

                }else{
                    getMvpView().hideLoading();
                    getMvpView().error(mContext.getResources().getString(R.string.server_error));
                }
            }
            @Override
            public void onFailure(Call<CommonResponseModel<LoginResponseModel>> call, Throwable t)
            {
                call.cancel();
                if (t instanceof IOException) {
                    getMvpView().error(mContext.getResources().getString(R.string.internet_connection_issue));
                }
                else {
                    getMvpView().error(mContext.getResources().getString(R.string.server_error));
                }
                getMvpView().hideLoading();
            }
        });
    }
}
