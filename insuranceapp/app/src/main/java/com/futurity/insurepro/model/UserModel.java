package com.futurity.insurepro.model;

import android.app.NotificationManager;
import android.content.Context;

import com.google.gson.Gson;
import com.futurity.insurepro.components.GlobalConstant;
import com.futurity.insurepro.util.SharedPreference;

public class UserModel {
    public static final int USER_CURRENT_ACTIVITY_IN_PROGRESS=1;
    public static final int USER_CURRENT_ACTIVITY_NOT_STARTED=2;
    public static final int USER_CURRENT_ACTIVITY_ABSENT=3;
    public static final int USER_CURRENT_ACTIVITY_DONE=5;
    public static final int USER_CURRENT_ACTIVITY_LEAVE=4;
    public static final int USER_CURRENT_ACTIVITY_WEEKLY_OFF=6;
    public static final int USER_CURRENT_ACTIVITY_LOCAL_PUNCH=7;
    public static final int CLASS_TYPE_1=1;
    public static final int CLASS_TYPE_2=2;
    public static final int CLASS_TYPE_3=3;
    public static final int CLASS_TYPE_4=4;

    public UserModel() {
    }
    public static void login(Context context, UserProfileModel user){
        Gson gson = new Gson();
        String json = gson.toJson(user);
        SharedPreference sp = new SharedPreference(context);
        sp.setValueBool(GlobalConstant.KEY_ISLOGIN, true);
        sp.setValueString(GlobalConstant.USER_OBJ,json);
    }
    public static void updateUser(Context context, UserProfileModel user){
        Gson gson = new Gson();
        String json = gson.toJson(user);
        SharedPreference sp = new SharedPreference(context);
        sp.setValueString(GlobalConstant.USER_OBJ,json);
    }
    public static boolean isLogin(Context context){
        SharedPreference sp = new SharedPreference(context);
        String userObjString=sp.getValueString(GlobalConstant.USER_OBJ);
        if(userObjString.length()>0){
            return sp.getValueBoolean(GlobalConstant.KEY_ISLOGIN);
        }
        return false;
    }
    public static void logout(Context context){
        SharedPreference sp = new SharedPreference(context);
        sp.setValueBool(GlobalConstant.KEY_ISLOGIN, false);
        sp.clearAll();
        clearAllNotifications(context);

    }
    public static void clearAllNotifications(Context context){
        NotificationManager nMgr = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        nMgr.cancelAll();
    }
    public static UserProfileModel getUser(Context context){
        SharedPreference sp = new SharedPreference(context);
        String userObjString=sp.getValueString(GlobalConstant.USER_OBJ);

        Gson gson = new Gson();
        UserProfileModel user = gson.fromJson(userObjString, UserProfileModel.class);
        return user;
    }

    public static void firstTimeAskingPermission(Context context,boolean val){
        SharedPreference sp = new SharedPreference(context);
        sp.setValueBool(GlobalConstant.FIRST_TIME_PERMISSION_LOCATION,val);
    }
    public static boolean isFirstTimeAskingPermission(Context context){
        SharedPreference sp = new SharedPreference(context);
        boolean val=sp.getPermissionBoolean(GlobalConstant.FIRST_TIME_PERMISSION_LOCATION);
        return val;
    }
    public static void firstTimeAskingPermission3(Context context,boolean val){
        SharedPreference sp = new SharedPreference(context);
        sp.setValueBool(GlobalConstant.FIRST_TIME_PERMISSION_STORAGE,val);
    }
    public static boolean isFirstTimeAskingPermission3(Context context){
        SharedPreference sp = new SharedPreference(context);
        boolean val=sp.getPermissionBoolean(GlobalConstant.FIRST_TIME_PERMISSION_STORAGE);
        return val;
    }
    public static void firstTimeAskingPermission1(Context context,boolean val){
        SharedPreference sp = new SharedPreference(context);
        sp.setValueBool(GlobalConstant.FIRST_TIME_PERMISSION_READ_PHONE_STATE,val);
    }
    public static boolean isFirstTimeAskingPermission1(Context context){
        SharedPreference sp = new SharedPreference(context);
        boolean val=sp.getPermissionBoolean(GlobalConstant.FIRST_TIME_PERMISSION_READ_PHONE_STATE);
        return val;
    }
    public static void firstTimeAskingPermission2(Context context,boolean val){
        SharedPreference sp = new SharedPreference(context);
        sp.setValueBool(GlobalConstant.FIRST_TIME_PERMISSION_READ_CALL_LOGS,val);
    }
    public static boolean isFirstTimeAskingPermission2(Context context){
        SharedPreference sp = new SharedPreference(context);
        boolean val=sp.getPermissionBoolean(GlobalConstant.FIRST_TIME_PERMISSION_READ_CALL_LOGS);
        return val;
    }

    public static void setDeviceTokeSendSuccess(Context context, String token){
        SharedPreference sp = new SharedPreference(context);
        sp.setValueString(GlobalConstant.DEVICE_TOKEN_SUCCESS, token);
    }
    public static String getDeviceTokeSendSuccess(Context context){
        SharedPreference sp = new SharedPreference(context);
        return sp.getValueString(GlobalConstant.DEVICE_TOKEN_SUCCESS);
    }

    public static void setWindowLayout(Context context,boolean val){
        SharedPreference sp = new SharedPreference(context);
        sp.setValueBool(GlobalConstant.SET_WINDOW_LAYOUT,val);
    }
    public static boolean getWindowLayout(Context context){
        SharedPreference sp = new SharedPreference(context);
        boolean val=sp.getBooleanVal(GlobalConstant.SET_WINDOW_LAYOUT);
        return val;
    }
    public static void setRid(Context context,int val){
        SharedPreference sp = new SharedPreference(context);
        sp.setValueInt(GlobalConstant.SET_RID,val);
    }
    public static int getRid(Context context){
        SharedPreference sp = new SharedPreference(context);
        int val=sp.getValueInt(GlobalConstant.SET_RID);
        return val;
    }

}
