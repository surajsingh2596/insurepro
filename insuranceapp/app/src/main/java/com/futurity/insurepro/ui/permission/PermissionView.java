package com.futurity.insurepro.ui.permission;


import com.futurity.insurepro.model.PermissionModel;
import com.futurity.insurepro.ui.base.BaseView;

import java.util.List;

public interface PermissionView extends BaseView {
    void getPermissionList(List<PermissionModel> permissionModelList);
    void setPermissionType(String permission, int type);
    void hideAllButtons();
    void showButtonEnabled();
    void showButtonDisabled();
    void showButtonDisabledSetting();
    void hideButtonEnabled();
    void hideButtonDisabled();
    void hideButtonDisabledSetting();

    void showOverLayEnabled();
    void hideOverlayEnabled();
    void showOverlayDisabled();
    void hideOverlayDisabled();
    void hideAllOverlayButtons();
    void onClickEnable();
    void onClickDisable();
    void onClickDisableSetting();
    void onClickOverlay();
    void getAllPermit(boolean b);
}
