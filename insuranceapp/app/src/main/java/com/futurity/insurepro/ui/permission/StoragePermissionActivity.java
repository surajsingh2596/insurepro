package com.futurity.insurepro.ui.permission;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.futurity.insurepro.R;
import com.futurity.insurepro.model.UserModel;


public class StoragePermissionActivity extends AppCompatActivity {
    private static String TAG="Camera";
    private MarshMallowPermission marshMallowPermission=new MarshMallowPermission(this);
    private TextView cameraText;
    private ImageView cameraIcon;
    private boolean isLocation=false;
    private boolean ifNeverAskAgain=false;
    private boolean isLocationStart=false;
    private boolean isLocationDialog=true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.storage_permission_activity);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Window w = getWindow();
            w.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            w.setStatusBarColor(getColor(R.color.theme_color_dark));
        }
        cameraIcon=(ImageView) findViewById(R.id.camera_icon);
        cameraText=(TextView) findViewById(R.id.camera_icon_text);
        //checkPermission();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if( isLocationStart){
            Intent resultIntent = new Intent();
            setResult(Activity.RESULT_OK, resultIntent);
        }else{
            Intent resultIntent = new Intent();
            setResult(Activity.RESULT_CANCELED, resultIntent);

        }
    }

    @Override
    public void onResume(){
        super.onResume();

        if(!marshMallowPermission.checkPermissionForExternalStorage()){
            isLocation=false;
            if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)){
                ifNeverAskAgain=false;
            }else{
                // set to never ask again
                if(UserModel.isFirstTimeAskingPermission3(this)){
                    ifNeverAskAgain=false;
                    UserModel.firstTimeAskingPermission3(this,false);
                }else{
                    ifNeverAskAgain=true;
                }

            }
        }else{
            isLocation=true;
            ifNeverAskAgain=false;
            isLocationStart=true;
        }

        closePermissionActivity();
    }
    public void closePermissionActivity(){
        if(isLocation && isLocationStart){
            Log.d(TAG, "onResume: final ");
            Intent resultIntent = new Intent();
            setResult(Activity.RESULT_OK, resultIntent);
            finish();
        }
    }
    public void enableCameraAccess(View view){
        if(!isLocation && ifNeverAskAgain){
            openSettings();
        }else{
            marshMallowPermission.requestPermissionForExternalStorage();
        }

    }
    private boolean checkIfNeverAskAgainSelected(String permission){
        if(!ActivityCompat.shouldShowRequestPermissionRationale(this,permission)){
            if(ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED){
                //allowed
                return false;
            }else{
                return true;
            }
        }
        return false;
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Log.d(TAG, "onRequestPermissionsResult: "+permissions[0]);
        switch (requestCode) {
            case MarshMallowPermission.EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE:
                if(ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[0])){
                    //denied
                    ifNeverAskAgain=false;
                    Log.d(TAG, "onRequestPermissionsResult: rational");
                }else{
                    if(ActivityCompat.checkSelfPermission(this, permissions[0]) == PackageManager.PERMISSION_GRANTED){
                        //allowed
                        Log.d(TAG, "onRequestPermissionsResult: granted");
                        isLocation=true;
                        ifNeverAskAgain=false;
                    } else{
                        //set to never ask again
                        Log.d(TAG, "onRequestPermissionsResult: granted not");
                        isLocation=false;
                        ifNeverAskAgain=true;
                        //openSettings();
                    }
                }
                break;
        }
    }
    private void openSettings(){
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivity(intent);
    }
    /*private void setDisableBgLocation(){
        cameraIcon.setBackground(getResources().getDrawable(R.mipmap.location));
        cameraText.setText(getString(R.string.location_enable_bt));
        cameraText.setTextColor(getResources().getColor(R.color.colorPrimary));
        isLocationStart=false;

    }*/

}
