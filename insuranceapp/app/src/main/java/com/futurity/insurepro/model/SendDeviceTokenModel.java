package com.futurity.insurepro.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SendDeviceTokenModel {
    @SerializedName("device_token")
    @Expose
    private String deviceToken;

    public SendDeviceTokenModel() {
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }
}
