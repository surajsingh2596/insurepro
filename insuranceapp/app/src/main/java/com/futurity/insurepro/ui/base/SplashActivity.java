package com.futurity.insurepro.ui.base;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.futurity.insurepro.R;
import com.futurity.insurepro.components.GlobalConstant;
import com.futurity.insurepro.model.UserModel;
import com.futurity.insurepro.ui.login.LoginActivity;
import com.futurity.insurepro.ui.main.MainActivity;
import com.futurity.insurepro.ui.sendOtp.SendOtpActivity;

public class SplashActivity extends AppCompatActivity {

    private static String TAG="Splash activity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
       // startService(new Intent(this, StartBroadcastService.class));
        startTimer();
    }
    private void startTimer()
    {
        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    checkIfUserLogin();
                }
                catch (Exception e)
                {
                    Log.d("qweqweqwe", "run: ");
                    e.printStackTrace();
                }
            }
        }, 3000);
    }
    private void checkIfUserLogin(){
        if(UserModel.isLogin(this)){
            goToDashboard();
        }else{
            loginMobileActivity();
        }
    }
    private void loginMobileActivity(){
        Class<?> aClass=null;
        if(GlobalConstant.LOGIN_TYPE==1){
            aClass=LoginActivity.class;
        }else if(GlobalConstant.LOGIN_TYPE==2){
            aClass= SendOtpActivity.class;
        }

        if(aClass!=null){
            Intent i = new Intent(SplashActivity.this, aClass);
            startActivity(i);
            finish();
        }

    }
    private void goToDashboard(){

        Intent i = new Intent(SplashActivity.this, MainActivity.class);
        startActivity(i);
        finish();
    }

}
