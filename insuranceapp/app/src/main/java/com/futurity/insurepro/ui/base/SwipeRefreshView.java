package com.futurity.insurepro.ui.base;


public interface SwipeRefreshView extends BaseView{
    void onSwipeRefresh();
    void onCloseSwipeRefresh();

}
