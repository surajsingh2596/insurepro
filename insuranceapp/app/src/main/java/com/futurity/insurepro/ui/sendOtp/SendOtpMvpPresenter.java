
package com.futurity.insurepro.ui.sendOtp;


import com.futurity.insurepro.model.SendOtpModel;
import com.futurity.insurepro.ui.base.MvpPresenter;

public interface SendOtpMvpPresenter<V extends SendOtpView> extends MvpPresenter<V> {
   void sendOtp(SendOtpModel sendOtpModel);
}
